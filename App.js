/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import 'react-native-gesture-handler';
import React, { useEffect, useState } from 'react';
import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
//import { AnimatedTabBarNavigator } from "react-native-animated-nav-tab-bar";
import { ScrollView, TouchableOpacity, Text, StyleSheet, View, Image, } from 'react-native';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import Login from './src/auth/Login';
import { Loader } from './src/components/Loader';
import TermsAndConditions from './src/components/TermsAndConditions';
import Privacy from './src/components/Privacy';
import Support from './src/components/Support';
import HomeScreen from './src/components/HomeScreen';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Otp from './src/auth/Otp';
import SignUp from './src/auth/SignUp';
import SetMpin from './src/auth/SetMpin';
import DetailScreen from './src/components/DetailScreen';
import GetProfile from './src/auth/GetProfile';
import BottomTab from './src/components/BottomTab';
import DetailScreenMenu from './src/components/DetailScreenMenu';
import BottomTabHomeScreen from './src/components/BottomTabHomeScreen';
import ResponsiveSize from './src/config/ResponsiveSize';
import SubjectList from './src/components/SubjectList';
import TimeTable from './src/components/TimeTable';

const navOptionHandler = () => ({
  headerShown: false,
});



const Drawer = createDrawerNavigator();
function DrawerNavigator() {
  const [Phone, setPhone] = useState("");
  const [Name, setName] = useState("");
  useEffect(() => {
    //this.retrieveData();
  });
  clearAll = async (props) => {
    try {
      const value = await AsyncStorage.clear();
      console.log(value);
      props.navigation.navigate("SignUp");
    } catch (error) {
      console.log(error);
    }
  };
  retrieveData = async key => {

    try {
      const value = await AsyncStorage.getItem('phone');
      if (value !== null) {
        // alert (value);
        setPhone(value)
      }
    } catch (error) {
      alert('Error retrieving data');
    }

    try {
      const value = await AsyncStorage.getItem('name');
      if (value !== null) {
        // alert (value);
        setName(value)
      }
    } catch (error) {
      alert('Error retrieving data');
    }
  }
  return (
    <Drawer.Navigator statusBarAnimation="slide"
      //initialRouteName="Dashboard"
      drawerContent={props =>
        <View style={{ width: "100%", height: "100%", backgroundColor: "#23C4D7" }}>
          {/*<Image style={{ height: "30%", width: "100%", resizeMode: "cover", alignSelf: "center", position: "absolute" }} source={require("./src/assets/bg.jpg")} />*/}
          <View style={{ height: "20%", marginTop: 30, width: "90%", alignSelf: "center", borderBottomWidth: 1, flexDirection: "row", justifyContent: "center", alignItems: "center" }} >
            <View style={{ width: 80, height: 80, borderRadius: 200, borderWidth: 2, borderColor: "white", justifyContent: "center", alignItems: "center" }}>
              <Image style={{ width: 78, height: 78, alignSelf: "center", resizeMode: "contain", borderRadius: 200, }} source={require("./src/assets/user.png")} />
            </View>
            <View style={{ width: "65%", minHeight: "30%", justifyContent: "center", alignItems: "center" }}>
              <Text numberOfLines={2} style={{ marginBottom: 5, fontSize: 22, fontWeight: "bold" }}>{Name}Username</Text>
              <Text numberOfLines={1} style={{ fontWeight: "bold" }}>{Phone}9876543210</Text>
            </View>
          </View>

          <ScrollView style={{ marginLeft: 5, marginTop: 50 }}>
            <TouchableOpacity
              style={Styles.button}
              onPress={() => {
                props.navigation.navigate("DetailScreenMenu", { title: "Student Profile" })
              }}
            >
              <View style={{ justifyContent: "center", alignItems: "center", flexDirection: "row" }}>
                {/*<Thumbnail square style={{ width: 25, height: 25, resizeMode: "contain" }} source={require('./src/assets/home.png')} ></Thumbnail>*/}
                <Text style={Styles.text}>Student Profile</Text>
              </View>
              {/*<Thumbnail square style={{ width: 20, height: 20, resizeMode: "contain", marginRight: 10 }} source={require('./src/assets/rightArrow.png')} ></Thumbnail>*/}
            </TouchableOpacity>
            <TouchableOpacity
              style={Styles.button}
              onPress={() => {
                props.navigation.navigate("DetailScreenMenu", { title: "Subjects" })
              }}
            >
              <View style={{ justifyContent: "center", alignItems: "center", flexDirection: "row" }}>
                <Text style={Styles.text}>Subjects</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={Styles.button}
              onPress={() => {
                props.navigation.navigate("DetailScreenMenu", { title: "Time Table" })
              }}
            >
              <View style={{ justifyContent: "center", alignItems: "center", flexDirection: "row" }}>
                <Text style={Styles.text}>Time Table</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={Styles.button}
              onPress={() => {
                props.navigation.navigate("DetailScreenMenu", { title: "Attendance" })
              }}
            >
              <View style={{ justifyContent: "center", alignItems: "center", flexDirection: "row" }}>
                <Text style={Styles.text}>Attendance</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={Styles.button}
              onPress={() => {
                props.navigation.navigate("DetailScreenMenu", { title: "Internal Marks" })
              }}
            >
              <View style={{ justifyContent: "center", alignItems: "center", flexDirection: "row" }}>
                <Text style={Styles.text}>Internal Marks</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={Styles.button}
              onPress={() => {
                props.navigation.navigate("DetailScreenMenu", { title: "University Marks" })
              }}
            >
              <View style={{ justifyContent: "center", alignItems: "center", flexDirection: "row" }}>
                <Text style={Styles.text}>University Marks</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={Styles.button}
              onPress={() => {
                props.navigation.navigate("DetailScreenMenu", { title: "Performance" })
              }}
            >
              <View style={{ justifyContent: "center", alignItems: "center", flexDirection: "row" }}>
                <Text style={Styles.text}>Performance</Text>
              </View>
            </TouchableOpacity>


          </ScrollView>

          <TouchableOpacity style={{ marginLeft: 20, marginBottom: 10 }}
            onPress={() => {
              props.navigation.navigate("Privacy")
            }}>
            <Text style={Styles.text2}>Privacy policy </Text>
          </TouchableOpacity>
          <TouchableOpacity style={{ marginBottom: 40, marginLeft: 20 }}
            onPress={() => {
              props.navigation.navigate("TermsAndConditions")
            }}>
            <Text style={Styles.text2}>Terms of use </Text>
          </TouchableOpacity>
        </View>
      }
    //  drawerContent={() => <CustomDrawerContent/>}
    >
      <Drawer.Screen name="BottomTabHomeScreen" component={BottomTabHomeScreen} />
      <Drawer.Screen name="DetailScreen" component={DetailScreen} />
      <Drawer.Screen name="HomeScreen" component={HomeScreen} />
    </Drawer.Navigator>
  );
}


//const Tab = createBottomTabNavigator();
////const Tab = AnimatedTabBarNavigator();
//function TabNavigator() {
//  return (
//    <Tab.Navigator>
//      <Tab.Screen
//        component={HomeScreen}
//        name="BottomTab"
//      >
//      </Tab.Screen>
//    </Tab.Navigator>
//  );
//}


const StackHome = createStackNavigator();
function HomeStack() {
  return (
    <StackHome.Navigator initialRouteName="SignUp">
      <StackHome.Screen
        name="Login"
        component={Login}
        options={navOptionHandler}
      />
      <StackHome.Screen
        name="Otp"
        component={Otp}
        options={navOptionHandler}
      />
      <StackHome.Screen
        name="SignUp"
        component={SignUp}
        options={navOptionHandler}
      />
      <StackHome.Screen
        name="SetMpin"
        component={SetMpin}
        options={navOptionHandler}
      />
      <StackHome.Screen
        name="GetProfile"
        component={GetProfile}
        options={navOptionHandler}
      />


    </StackHome.Navigator>
  );
}

const StackHome2 = createStackNavigator();
function HomeStack2() {
  return (
    <StackHome2.Navigator initialRouteName="Drawer">

      <StackHome2.Screen
        name="Drawer"
        component={DrawerNavigator}
        options={navOptionHandler}
      />
      <StackHome2.Screen
        name="Loader"
        component={Loader}
        options={navOptionHandler}
      />
      <StackHome2.Screen
        name="BottomTab"
        component={BottomTab}
        options={navOptionHandler}
      />
      <StackHome2.Screen
        name="TermsAndConditions"
        component={TermsAndConditions}
        options={navOptionHandler}
      />
      <StackHome2.Screen
        name="Privacy"
        component={Privacy}
        options={navOptionHandler}
      />
      <StackHome2.Screen
        name="Support"
        component={Support}
        options={navOptionHandler}
      />
      <StackHome2.Screen
        name="SubjectList"
        component={SubjectList}
        options={navOptionHandler}
      />
      <StackHome2.Screen
        name="DetailScreenMenu"
        component={DetailScreenMenu}
        options={navOptionHandler}
      />
      <StackHome2.Screen
        name="TimeTable"
        component={TimeTable}
        options={navOptionHandler}
      />


    </StackHome2.Navigator>
  );
}



const StackApp = createStackNavigator();
export default function App() {
  return (
    <SafeAreaProvider>
      <NavigationContainer>
        <StackApp.Navigator mode="modal" initialRouteName="HomeApp">
          {/*<StackApp.Navigator mode="modal" initialRouteName="HomeStack2">*/}
          <StackApp.Screen
            name="HomeApp"
            component={HomeStack}
            options={navOptionHandler}
          />
          <StackApp.Screen
            name="HomeStack2"
            component={HomeStack2}
            options={navOptionHandler}
          />
        </StackApp.Navigator>
      </NavigationContainer>
    </SafeAreaProvider>
  )
};
const Styles = StyleSheet.create({
  button: {
    width: '98%',
    alignSelf: "center",
    paddingVertical: 8,
    marginTop: 10,
    marginLeft: 10,
    //backgroundColor: '#EFEFEF',
    flexDirection: 'row',
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: "space-between"
  },
  text: {
    fontWeight: "500",
    marginLeft: 20,
    fontSize: ResponsiveSize(14),

  },
  text2: {
    fontWeight: "400",
    marginLeft: 20
  }
})
