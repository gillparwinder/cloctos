/**
 * @format
 */
import 'react-native-gesture-handler';
import React from 'react';
import { AppRegistry } from 'react-native';
import App from './App';
import { name as appName } from './app.json';
import configureStore from './src/store';
import { Provider } from 'react-redux';
import GetProfile from './src/auth/GetProfile';
import DetailScreen from './src/components/DetailScreen';

const store = configureStore();

const MainApp = () =>
    <Provider store={store}>
        <App />
    </Provider>

AppRegistry.registerComponent(appName, () => MainApp);
