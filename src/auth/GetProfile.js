import { CheckBox, Content } from "native-base";
import React, { Component } from "react";
import { SafeAreaView } from "react-native";
import {
    Text,
    View,
    TouchableOpacity,
    TextInput,
    Image,
    StyleSheet,
    ImageBackground,
    Dimensions, Animated, Keyboard, KeyboardAvoidingView
} from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { ScrollView } from "react-native-gesture-handler";
import { CodeField, Cursor } from "react-native-confirmation-code-field";
const ScreenWidth = Dimensions.get("window").width;
const ScreenHeight = Dimensions.get("window").height;
import Toast from 'react-native-tiny-toast';
import { Loader } from "../components/Loader";
import config from "../config/config";
import ResponsiveSize from "../config/ResponsiveSize";
import { loginDetail } from "../actions/Detail";
import { connect } from "react-redux";
import { CustomHeader } from "../components/CustomHeader";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { Alert } from "react-native";
const ToastData = {
    position: Toast.position.TOP,
    imgSource: require('../assets/alert.png'),
    imgStyle: { width: 40, height: 40 },
};
class GetProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            InstituteCode: this.props.route.params ? (this.props.route.params.instituteCode ? this.props.route.params.instituteCode : "") : "",
            Mobile: this.props.route.params ? (this.props.route.params.mobileNumber ? this.props.route.params.mobileNumber : "") : "",
            HashKey: this.props.route.params ? (this.props.route.params.hashKey ? this.props.route.params.hashKey : "") : "",
            ShowInstituteCode: false,
            //InstituteCode: "SPUNI001",
            //Email: "abc@gmail.com",
            //Mobile: "9814469661",
            //InstituteCode: "SPUNI001",
            Loader: true,
            Email: "",
            Token: "",
            secureTextEntry: true
        };
    }

    storeData = async (key, value) => {
        try {
            await AsyncStorage.setItem(key, value);
            console.log('key==' + key + ', value==' + value);
        } catch (error) {
            // Error saving data
            alert('store data catched');
        }
    };
    componentDidMount() {
        this.onLoadGetDataByProfile()
        //console.log(this.props.route.params);
    }

    retrieveData = async key => {
        try {
            const value = await AsyncStorage.getItem('token');
            this.setState({ Loader: false }, function () {

            })
            if (value !== null) {
                // alert (value);
                this.setState({ Token: value, }, function () {
                    this.props.navigation.navigate("Login")
                });
            }
        } catch (error) {
            this.setState({ Loader: false })
            alert('Error retrieving data');
        }
    }
    onLoadGetDataByProfile = () => {
        const url = config.baseUrl + "usermanagment/StudentInfoByMobileNumber";
        console.log(url);
        console.log("instituteCode", this.state.InstituteCode,
            "emailId", this.state.Email,
            "mobileNumber", this.state.Mobile);
        fetch(url, {
            method: 'POST',
            headers: {
                "Accept": 'application/json',
                'Content-Type': 'application/json',
                //'Authorization': 'Bearer ' + this.state.Token
            },
            body: JSON.stringify({
                "instituteCode": this.state.InstituteCode,
                "emailId": this.state.Email,
                "mobileNumber": this.state.Mobile
            }),
        })
            .then(response => response.json())
            .then(responseJson => {
                //alert(JSON.stringify(responseJson))
                console.log(responseJson);
                if (responseJson.status == true) {
                    this.setState({ Loader: false, isValidUser: true, StudentData: responseJson.studentInfo }, function () {
                        //Toast.showSuccess(responseJson.message, { duration: 50, position: Toast.position.CENTER })
                        //this.props.navigation.navigate("Login")
                    })
                } else if (responseJson.status == false) {
                    this.setState({ Loader: false, isValidUser: false }, function () {
                        Alert.alert(
                            "Alert",
                            "No data available with these details.Please check and try again",
                            [

                                { text: "Go Back", onPress: () => this.props.navigation.goBack() }
                            ],
                            { cancelable: false }
                        );
                        //Toast.showSuccess(responseJson.message, { duration: 50, position: Toast.position.CENTER })
                        //this.props.navigation.navigate("Login")
                    })
                } else {
                    this.setState({ Loader: false, isValidUser: false }, function () {
                        Toast.show(responseJson.title ? responseJson.title : 'Temporary error try again after some time', ToastData);
                    })
                }
            })
            .catch(error => {
                this.setState({ Loader: false, isValidUser: false }, function () {
                    //alert(JSON.stringify(error))
                    //setTimeout(() => {
                    //    this.props.navigation.goBack()
                    //}, 1000);

                    Alert.alert(
                        "Alert",
                        "No data available with these details.Please check and try again",
                        [

                            { text: "Go Back", onPress: () => this.props.navigation.goBack() }
                        ],
                        { cancelable: false }
                    );
                    console.log(error);
                    Toast.show("No data available with these details.", ToastData);
                })
            });
    }

    submit = () => {
        var phoneno = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
        const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        console.log(this.state.Mobile.match(phoneno));
        if (this.state.Mobile == '') {
            this.setState({ Loader: false }, function () {
                Toast.show('Mobile number required', ToastData);
            })
        } else if (this.state.Mobile.match(phoneno) == null) {
            this.setState({ Loader: false }, function () {
                Toast.show('Enter valid Mobile number', ToastData);
            })
            //} else if (this.state.Email == '' || reg.test(this.state.Email) === false) {
            //    this.setState({ Loader: false }, function () {
            //        Toast.show(
            //            this.state.Email == ''
            //                ? 'Email is required'
            //                : 'Enter valid email address',
            //            ToastData
            //        );
            //    })
        } else {
            const url = config.baseUrl + "usermanagment/GenerateOTP";
            console.log(url);
            fetch(url, {
                method: 'POST',
                headers: {
                    "Accept": 'application/json',
                    'Content-Type': 'application/json',
                    //'Authorization': 'Bearer ' + this.state.Token
                },
                body: JSON.stringify({
                    "instituteCode": this.state.InstituteCode,
                    "emailId": this.state.Email,
                    "mobileNumber": this.state.Mobile,
                    "hashKey": this.state.HashKey
                }),
            })
                .then(response => response.json())
                .then(responseJson => {
                    //alert(JSON.stringify(responseJson))
                    console.log(responseJson);
                    if (responseJson && responseJson.status == true) {
                        this.setState({ Loader: false }, function () {
                            //this.storeData('mobile', this.state.Mobile);
                            this.storeData('instituteCode', this.state.InstituteCode);
                            this.storeData('email', this.state.Email);
                            this.storeData('mobile', this.state.Mobile);
                            this.props.navigation.navigate("Otp", {
                                instituteCode: this.state.InstituteCode,
                                emailId: this.state.Email,
                                mobileNumber: this.state.Mobile,
                                hashKey: this.state.HashKey
                            })
                            this.props.loginDetail(responseJson)
                        })
                    } else if (responseJson && responseJson.status == false) {
                        this.setState({ Loader: false }, function () {

                            Toast.show(responseJson.message, ToastData);
                        })
                    } else {
                        this.setState({ Loader: false }, function () {
                            Toast.show(responseJson.title ? responseJson.title : 'Temporary error try again after some time', ToastData);
                        })
                    }
                })
                .catch(error => {
                    this.setState({ Loader: false }, function () {
                        //alert(JSON.stringify(error))
                        console.log(error);
                        Toast.show("error", ToastData);
                    })
                });
        }
    };

    render() {
        return (
            <KeyboardAwareScrollView
                //style={{ backgroundColor: '#4c69a5' }}
                resetScrollToCoords={{ x: 0, y: 0 }}
                contentContainerStyle={{ height: ScreenHeight, display: "flex" }}
                scrollEnabled={true}
            >
                <Loader
                    Loading={this.state.Loader ? true : false}
                />
                <SafeAreaView style={styles.mainContainer}>
                    <TouchableOpacity onPress={() => { this.props.navigation.goBack() }} style={{ width: 150, height: 150, alignSelf: "flex-start", position: "absolute", zIndex: 2 }} />
                    <View style={styles.ImageConatiner}>
                        <Image style={{ width: "100%", height: "100%", resizeMode: "stretch" }} source={require("../assets/createPinLogo2.png")} />
                    </View>
                    <View style={styles.emailConatiner}>
                        <Text style={{ fontFamily: "Poppins-Regular", color: "white", marginTop: 30, fontWeight: "600", fontSize: ResponsiveSize(config.textSize) }}>Your Profile</Text>
                        <View style={{ width: "80%", height: 55, justifyContent: "space-between", marginTop: 20 }}>
                            <Text style={{ fontFamily: "Poppins-Regular", color: "black", fontSize: ResponsiveSize(11) }}>Student name</Text>
                            <Text style={{ fontFamily: "Poppins-Regular", color: "white", fontSize: ResponsiveSize(12), fontWeight: "600" }}>{this.state.StudentData ? this.state.StudentData.name : ""}</Text>
                        </View>
                        <View style={styles.emailOuter}>
                            <View style={styles.titleContainer}>
                                <Text style={styles.title}>Batch year</Text>
                                <Text style={styles.titleData}>{this.state.StudentData ? this.state.StudentData.batchYear : ""}</Text>
                            </View>
                            <View style={styles.titleContainer}>
                                <Text style={styles.title}>Degree</Text>
                                <Text style={styles.titleData}>{this.state.StudentData ? this.state.StudentData.courseName : ""}</Text>
                            </View>
                        </View>
                        <View style={styles.emailOuter}>
                            <View style={styles.titleContainer}>
                                <Text style={styles.title}>Registration number</Text>
                                <Text style={styles.titleData}>{this.state.StudentData ? this.state.StudentData.rollNo : ""}</Text>
                            </View>
                            <View style={styles.titleContainer}>
                                <Text style={styles.title}>Semester</Text>
                                <Text style={styles.titleData}>{this.state.StudentData ? this.state.StudentData.currentSemester : ""}</Text>
                            </View>
                        </View>
                        <TouchableOpacity style={[styles.loginButton, { marginTop: 40, alignSelf: "center" }]} onPress={() =>
                            this.setState({ Loader: true }, function () {
                                this.submit()
                            })
                        }>
                            <Text style={styles.text}>Generate OTP</Text>
                        </TouchableOpacity>
                    </View>
                </SafeAreaView>
            </KeyboardAwareScrollView>
        )
    }
}
const styles = StyleSheet.create({
    ImageConatiner: {
        width: "100%",
        alignItems: "center",
        justifyContent: "center",
        height: "50%",
    },
    titleContainer: {
        width: "50%",
        height: 55,
        justifyContent: "space-between"
    },
    title: {
        color: "black",
        fontSize: ResponsiveSize(11)
    },
    titleData: {
        color: "white",
        fontSize: ResponsiveSize(14),
        fontWeight: "600"
    },
    mainContainer: {
        width: "100%",
        flex: 1,
        alignItems: "center",
        backgroundColor: config.bgColor
    },
    text: {
        textAlign: "center",
        color: "white",
        fontSize: ResponsiveSize(14)
    },
    logoConatiner: {
        width: "100%",
        height: "35%",
        justifyContent: "center",
        alignItems: "center"
    },
    bannerLogoConatiner: {
        width: "100%",
        height: "25%",
        justifyContent: "center",
        alignItems: "center"
    },
    loginText: {
        //color: "white",
        fontWeight: "600",
        fontSize: 35
    },
    emailConatiner: {
        width: "100%",
        height: "55%",
        backgroundColor: config.SubThemeColor,
        borderTopLeftRadius: 60,
        borderTopRightRadius: 60,
        alignItems: "center"
    },
    emailOuter: {
        width: "80%",
        alignSelf: "center",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        minHeight: 55,
        borderRadius: 5,
        marginTop: 20,
    },
    emailInput: {
        height: 55,
        width: "90%",
        fontSize: ResponsiveSize(14),
        paddingLeft: 10,
        backgroundColor: config.SubThemeColor,
        color: "white",
        borderRadius: 35,
    },
    emailInputOuter: {
        height: 55,
        width: "80%",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "flex-start",
        backgroundColor: config.SubThemeColor,
        borderRadius: 35,
    },
    loginButton: {
        width: "45%",
        backgroundColor: config.themeColor,

        borderRadius: 25,
        justifyContent: "center",
        alignItems: "center",
        height: 45
    },
    buttonsContainer: {
        width: "100%",
        alignItems: "center",
        justifyContent: "space-evenly",
        flexDirection: "row",
        height: 55,
    },
    codeFieldRoot: {
        //marginTop: 90,
        width: "80%",
        alignSelf: "center"
    },
    cell: {
        width: 40,
        height: 40,
        lineHeight: 40,
        fontSize: ResponsiveSize(config.textSize),
        borderWidth: 2,
        borderRadius: 10,
        borderColor: '#00000030',
        textAlign: 'center',
    },
    focusCell: {
        shadowColor: "black",
        shadowOffset: { width: 1, height: 1 },
        shadowOpacity: 1,
        borderColor: "white",
        borderRadius: 10,
    },
});
const mapStateToProps = state => {
    console.log(state);
    return {
        //  Details: state.detailReducer.DetailList
    };
};

const mapDispatchToProps = (dispatch) => {
    //console.log("dispatch====" + dispatch);
    return {
        //delete: (key) => dispatch(deleteDetail(key))
        loginDetail: (key) => dispatch(loginDetail(key))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(GetProfile);
