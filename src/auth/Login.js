import AsyncStorage from "@react-native-async-storage/async-storage";
import { Content } from "native-base";
import React, { Component } from "react";
import { SafeAreaView } from "react-native";
import {
    Text,
    View,
    TouchableOpacity,
    TextInput,
    Image,
    StyleSheet,
    Dimensions,
} from "react-native";
import CodeInput from 'react-native-confirmation-code-input';
import { CodeField, Cursor, useBlurOnFulfill } from "react-native-confirmation-code-field";
import { ScrollView } from "react-native-gesture-handler";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
const ScreenWidth = Dimensions.get("window").width;
const ScreenHeight = Dimensions.get("window").height;
import Toast from 'react-native-tiny-toast';
import { Loader } from "../components/Loader";
import config from "../config/config";
import ResponsiveSize from "../config/ResponsiveSize";
import { StatusBar } from "react-native";
const ToastData = {
    position: Toast.position.TOP,
    imgSource: require('../assets/alert.png'),
    imgStyle: { width: 40, height: 40 },
};
let abortController = null;
export default class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Mpin: "",
            Loader: false

        };
    }
    clearAll = async (props) => {
        try {
            const value = await AsyncStorage.clear();
            console.log(value);
            props.navigation.navigate("Login");
        } catch (error) {
            console.log(error);
        }
    };
    storeData = async (key, value) => {
        try {
            await AsyncStorage.setItem(key, value);
            console.log('key==' + key + ', value==' + value);
        } catch (error) {
            // Error saving data
            alert('store data catched');
        }
    };
    componentDidMount() {
        this.retrieveData();
    }
    retrieveData = async key => {
        try {
            const value = await AsyncStorage.getItem('token');
            if (value !== null) {
                //alert(value);
                this.setState({ Token: value, Loader: false }, function () {
                    //this.props.navigation.navigate("Login")
                });
            }
        } catch (error) {
            this.setState({ Loader: false })
            alert('Error retrieving data');
        }
    }

    submit = () => {
        if (this.state.Mpin == '') {
            this.setState({ Loader: false }, function () {
                Toast.show('Please enter mpin', ToastData);
            })

        } else if (this.state.Mpin.length != 4) {
            this.setState({ Loader: false }, function () {
                Toast.show('Mpin should be 4 digit', ToastData);
            })

        } else {
            const url = config.baseUrl + "student/MobileLogin";
            console.log(url);
            console.log(this.state.Mpin);
            fetch(url, {
                method: 'POST',
                headers: {
                    "Accept": 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this.state.Token
                },
                body: JSON.stringify({
                    "mpin": this.state.Mpin
                }),
            })
                .then(response => response.json())
                .then(responseJson => {
                    //alert(JSON.stringify(responseJson))
                    console.log(responseJson);
                    if (responseJson && responseJson.status == true) {
                        Toast.showSuccess("Great! Login successfully", { duration: 50, position: Toast.position.TOP })
                        this.setState({ Loader: false, }, function () {
                            this.props.navigation.navigate("HomeStack2")
                        })
                    } else if (responseJson && responseJson.status == false) {
                        this.setState({ Loader: false }, function () {
                            Toast.show(responseJson.message, ToastData);
                        })
                    } else {
                        this.setState({ Loader: false }, function () {
                            Toast.show(responseJson.title ? responseJson.title : 'Temporary error try again after some time', ToastData);
                        })
                    }
                })
                .catch(error => {
                    this.setState({ Loader: false }, function () {
                        //alert(JSON.stringify(error))
                        console.log(error);
                        Toast.show("error", ToastData);
                    })
                });
        }
    };
    render() {
        return (
            <View style={{ backgroundColor: "white", flex: 1 }}>
                <StatusBar barStyle="light-content" />
                <Loader
                    Loading={this.state.Loader ? true : false}
                />
                <KeyboardAwareScrollView
                    //style={{ backgroundColor: '#4c69a5' }}
                    resetScrollToCoords={{ x: 0, y: 0 }}
                    contentContainerStyle={{ height: ScreenHeight, display: "flex" }}
                    scrollEnabled={true}
                >
                    <SafeAreaView style={styles.mainContainer}>

                        <View style={styles.ImageConatiner}>
                            <Image style={{ width: "100%", height: "100%", resizeMode: "contain" }} source={require("../assets/loginLogo.png")} />
                        </View>
                        <View style={styles.emailConatiner}>
                            <Text style={[styles.text, { color: config.themeColor }]}>Enter M-PIN</Text>

                            <View style={styles.emailOuter}>

                                <CodeField
                                    {...this.props}
                                    value={this.state.Mpin}
                                    onChangeText={(text) => this.setState({ Mpin: text }, function () { if (this.state.Mpin.length == 4) { this.setState({ Loader: true }, function () { this.submit() }) } })}
                                    cellCount={4}
                                    rootStyle={styles.codeFieldRoot}
                                    blurOnSubmit={true}
                                    returnKeyType="done"
                                    keyboardType="number-pad"
                                    textContentType="oneTimeCode"
                                    renderCell={({ index, symbol, isFocused }) => (
                                        <View key={Math.random()} style={{ backgroundColor: "#E9ECFE", borderRadius: 10 }}>
                                            <Text
                                                key={index}
                                                style={[styles.cell, isFocused && styles.focusCell]}
                                            >
                                                {(isFocused ? <Cursor /> : null),
                                                    symbol ? "*" : null
                                                }
                                            </Text>
                                        </View>
                                    )}
                                />
                            </View>
                            <Text onPress={() => { this.props.navigation.navigate("SignUp") }} style={[styles.text, { color: config.themeColor, fontSize: ResponsiveSize(12) }]}>Create New M-PIN</Text>

                            <TouchableOpacity style={[styles.loginButton, { marginTop: 50, }]} onPress={() =>
                                this.setState({ Loader: true }, function () {
                                    this.submit()
                                    //this.clearAll()
                                })
                            }>
                                <Text style={styles.text}>Next</Text>
                            </TouchableOpacity>
                            <Text style={[styles.text, { color: config.themeColor, fontSize: ResponsiveSize(12), marginTop: 10 }]}>Privacy Policy</Text>

                            {/*<TouchableOpacity style={[styles.loginButton, { backgroundColor: "transparent", marginTop: 10, }]} onPress={() =>
                                this.props.navigation.navigate("SignUp")
                            }>
                                <Text style={[styles.text, { color: config.themeColor, fontWeight: "bold" }]}>REGISTER</Text>
                            </TouchableOpacity>*/}
                        </View>

                    </SafeAreaView>
                </KeyboardAwareScrollView>
            </View>
        )
    }
}
const styles = StyleSheet.create({

    text: {
        textAlign: "center",
        color: "white",
        fontSize: ResponsiveSize(14)
    },
    emailConatiner: {
        width: "100%",
        alignItems: "center",
        justifyContent: "center",
        height: "45%",
    },

    emailOuter: {
        width: "80%",
        backgroundColor: "transparent",
        justifyContent: "center",
        alignItems: "center",
        height: 100,
    },
    loginButton: {
        width: "45%",
        backgroundColor: config.themeColor,

        borderRadius: 25,
        justifyContent: "center",
        alignItems: "center",
        height: 45
    },
    codeFieldRoot: {
        //marginTop: 90,
        width: "80%",
        alignSelf: "center"
    },
    cell: {
        width: 50,
        height: 50,
        lineHeight: 50,
        fontSize: ResponsiveSize(config.textSize),
        borderWidth: 2,
        borderRadius: 10,
        borderColor: '#00000030',
        textAlign: 'center'
    },
    focusCell: {
        borderColor: config.themeColor,
    },
    ImageConatiner: {
        width: "100%",
        alignItems: "center",
        justifyContent: "center",
        height: "55%",
    },
    mainContainer: {
        width: "100%",
        flex: 1,
        alignItems: "center",
        backgroundColor: config.bgColor
    },
});