import { Content } from "native-base";
import React, { Component } from "react";
import { SafeAreaView } from "react-native";
import {
    Text,
    View,
    TouchableOpacity,
    TextInput,
    Image,
    StyleSheet,
    ImageBackground,

    Dimensions, Animated, Keyboard, KeyboardAvoidingView
} from "react-native";
import { CodeField, Cursor } from "react-native-confirmation-code-field";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { ScrollView } from "react-native-gesture-handler";
import { CountdownCircleTimer } from 'react-native-countdown-circle-timer'
const ScreenWidth = Dimensions.get("window").width;
const ScreenHeight = Dimensions.get("window").height;
import Toast from 'react-native-tiny-toast';
import { CustomHeader } from "../components/CustomHeader";
import config from "../config/config";
import ResponsiveSize from "../config/ResponsiveSize";
import { Loader } from "../components/Loader";
import { loginDetail } from "../actions/Detail";
import { connect } from "react-redux";
import CodeInput from 'react-native-confirmation-code-input';
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { Overlay } from "react-native-elements";

const ToastData = {
    position: Toast.position.TOP,
    imgSource: require('../assets/alert.png'),
    imgStyle: { width: 40, height: 40 },
};
class Otp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            OTP: "",
            WelcomeOverlayVisible: false,
            ResendOtp: false,
            Loader: false,
            InstituteCode: this.props.route.params ? (this.props.route.params.instituteCode ? this.props.route.params.instituteCode : "") : "",
            Mobile: this.props.route.params ? (this.props.route.params.mobileNumber ? this.props.route.params.mobileNumber : "") : "",
            Email: this.props.route.params ? (this.props.route.params.emailId ? this.props.route.params.emailId : "") : "",
            HashKey: this.props.route.params ? (this.props.route.params.hashKey ? this.props.route.params.hashKey : "") : "",

        };
    }
    storeData = async (key, value) => {
        try {
            await AsyncStorage.setItem(key, value);
            console.log('key==' + key + ', value==' + value);
        } catch (error) {
            // Error saving data
            alert('store data catched');
        }
    };
    componentDidMount() {

    }

    ResendOtp = () => {
        var phoneno = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
        const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        console.log(this.state.Mobile.match(phoneno));
        if (this.state.InstituteCode == '') {
            this.setState({ Loader: false }, function () {
                Toast.show('Please enter Institute Code', ToastData);
            })

        } else if (this.state.Mobile == '') {
            this.setState({ Loader: false }, function () {
                Toast.show('Mobile number required', ToastData);
            })
        } else if (this.state.Mobile.match(phoneno) == null) {
            this.setState({ Loader: false }, function () {
                Toast.show('Enter valid Mobile number', ToastData);
            })
            //} else if (this.state.Email == '' || reg.test(this.state.Email) === false) {
            //    this.setState({ Loader: false }, function () {
            //        Toast.show(
            //            this.state.Email == ''
            //                ? 'Email is required'
            //                : 'Enter valid email address',
            //            ToastData
            //        );
            //    })
        } else {
            const url = config.baseUrl + "usermanagment/GenerateOTP";
            console.log(url);
            fetch(url, {
                method: 'POST',
                headers: {
                    "Accept": 'application/json',
                    'Content-Type': 'application/json',
                    //'Authorization': 'Bearer ' + this.state.Token
                },
                body: JSON.stringify({
                    "instituteCode": this.state.InstituteCode,
                    "emailId": this.state.Email,
                    "mobileNumber": this.state.Mobile,
                    "hashKey": this.state.HashKey
                }),
            })
                .then(response => response.json())
                .then(responseJson => {
                    //alert(JSON.stringify(responseJson))
                    console.log(responseJson);
                    if (responseJson && responseJson.status == true) {
                        this.setState({ Loader: false, OTP: "" }, function () {
                            Toast.showSuccess("OTP sent successfully", { duration: 50, position: Toast.position.CENTER })
                        })
                    } else if (responseJson && responseJson.status == false) {
                        this.setState({ Loader: false }, function () {
                            Toast.show(responseJson.message, ToastData);
                        })
                    } else {
                        this.setState({ Loader: false }, function () {
                            Toast.show(responseJson.title ? responseJson.title : 'Temporary error try again after some time', ToastData);
                        })
                    }
                })
                .catch(error => {
                    this.setState({ Loader: false }, function () {
                        //alert(JSON.stringify(error))
                        console.log(error);
                        Toast.show("error", ToastData);
                    })
                });
        }
    };
    submit = () => {
        //const reg = /^[0]?[789]\d{9}$/;
        //const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (this.state.OTP.length == "") {
            this.setState({ Loader: false }, function () {
                Toast.show("Pleese Enter OTP", ToastData)
            })
        } else if (this.state.OTP.length != 4) {
            this.setState({ Loader: false }, function () {
                Toast.show("OTP should be equal to 4 digit ", ToastData)
            })
        } else {
            const url = config.baseUrl + "usermanagment/VerifyMobileOTP";
            console.log(url);
            fetch(url, {
                method: 'POST',
                headers: {
                    "Accept": 'application/json',
                    'Content-Type': 'application/json',
                    //'Authorization': 'Bearer ' + this.state.Token
                },
                body: JSON.stringify({
                    "instituteCode": this.state.InstituteCode,
                    "emailId": this.state.Email,
                    "mobileNumber": this.state.Mobile,
                    "otp": this.state.OTP,
                }),
            })
                .then(response => response.json())
                .then(responseJson => {
                    //alert(JSON.stringify(responseJson))
                    console.log(responseJson);
                    if (responseJson && responseJson.status == true) {
                        this.setState({ Loader: false, WelcomeOverlayVisible: true }, function () {
                            //Toast.showSuccess("OTP verified")
                            this.storeData('token', responseJson.message);
                            //this.storeData('mobile', this.state.Mobile);
                            //this.storeData('instituteCode', this.state.InstituteCode);
                            //this.props.navigation.navigate("SetMpin")
                            //this.props.navigation.navigate("Otp", { data: this.state.Mobile })
                        })
                    } else if (responseJson && responseJson.status == false) {
                        this.setState({ Loader: false }, function () {
                            Toast.show(responseJson.message, ToastData);
                        })
                    } else {
                        this.setState({ Loader: false }, function () {
                            Toast.show(responseJson.title ? responseJson.title : 'Temporary error try again after some time', ToastData);
                        })
                    }
                })
                .catch(error => {
                    this.setState({ Loader: false }, function () {
                        //alert(JSON.stringify(error))
                        console.log(error);
                        Toast.show("error", ToastData);
                    })
                });
        }
    };
    render() {
        return (
            <View style={{ backgroundColor: "white", flex: 1 }}>
                <Loader
                    Loading={this.state.Loader ? true : false}
                />
                <KeyboardAwareScrollView
                    //style={{ backgroundColor: '#4c69a5' }}
                    resetScrollToCoords={{ x: 0, y: 0 }}
                    contentContainerStyle={{ height: ScreenHeight, display: "flex" }}
                    scrollEnabled={true}
                >
                    <SafeAreaView style={styles.mainContainer}>
                        <TouchableOpacity onPress={() => { this.props.navigation.goBack() }} style={{ width: 150, height: 150, alignSelf: "flex-start", position: "absolute", zIndex: 2 }} />
                        <View style={styles.ImageConatiner}>
                            <Image style={{ width: "100%", height: "100%", resizeMode: "stretch", alignSelf: "flex-end" }} source={require("../assets/otp2.png")} />
                        </View>
                        {/*<Text style={[styles.text, { color: config.themeColor, alignSelf: "flex-start", textAlign: "left", width: "90%", marginTop: 30, alignSelf: "center" }]}>OTP Verification</Text>*/}
                        <Text style={[styles.text, { color: config.themeColor, alignSelf: "center", textAlign: "center", width: "90%", marginTop: 30, alignSelf: "center" }]}>Enter your OTP</Text>

                        <View style={styles.emailOuter}>

                            <CodeField
                                {...this.props}
                                value={this.state.OTP}
                                onChangeText={(text) => this.setState({ OTP: text }, function () { if (this.state.OTP.length == 4) { this.setState({ Loader: true }, function () { this.submit() }) } })}
                                cellCount={4}
                                blurOnSubmit={true}
                                returnKeyType="done"
                                rootStyle={styles.codeFieldRoot}
                                keyboardType="number-pad"
                                textContentType="oneTimeCode"
                                renderCell={({ index, symbol, isFocused }) => (
                                    <View key={Math.random()} style={{ backgroundColor: "#E9ECFE", borderRadius: 10 }}>
                                        <Text
                                            key={index}
                                            style={[styles.cell, isFocused && styles.focusCell]}
                                        >
                                            {(isFocused ? <Cursor /> : null),
                                                symbol ? "*" : null
                                            }
                                        </Text>
                                    </View>
                                )}
                            />
                        </View>
                        <Text style={{ fontFamily: "Poppins-Regular", color: "gray", textAlign: "center", fontSize: ResponsiveSize(18) }}>OTP Sent to {this.state.Mobile}</Text>
                        <TouchableOpacity style={[styles.loginButton, { marginTop: 50, alignSelf: "center" }]} onPress={() =>
                            this.setState({ Loader: true }, function () {
                                this.submit()
                                //this.clearAll()
                            })
                        }>
                            <Text style={styles.text}>Verify</Text>
                        </TouchableOpacity>

                        <View style={styles.buttonsContainer}>

                            {this.state.ResendOtp ?
                                <TouchableOpacity style={[styles.loginButton, { backgroundColor: "transparent" }]} onPress={() =>
                                    this.setState({ ResendOtp: false, Loader: true }, function () {
                                        this.ResendOtp()
                                    })
                                }>
                                    <Text style={[styles.text, { color: config.themeColor }]}>Resend Otp</Text>
                                </TouchableOpacity>
                                :
                                <CountdownCircleTimer

                                    size={30}
                                    strokeWidth={3}
                                    isPlaying={this.state.ResendOtp ? false : true}
                                    duration={20}
                                    onComplete={() => {
                                        this.setState({ ResendOtp: true, })
                                    }}
                                    colors={[
                                        [config.themeColor, 0.4],
                                        [config.SubThemeColor, 0.2],
                                        ['#A30000', 0.2],
                                    ]}
                                >
                                    {({ remainingTime, animatedColor }) => (
                                        <Animated.Text style={{ color: animatedColor }}>
                                            {remainingTime}
                                        </Animated.Text>
                                    )}
                                </CountdownCircleTimer>
                                //<Text style={[styles.text, { color: "black" }]}>{this.state.Time}</Text>
                            }
                        </View>
                    </SafeAreaView>
                    <Overlay visible={this.state.WelcomeOverlayVisible} overlayStyle={{ width: ScreenWidth - 150, height: ScreenHeight / 3, borderRadius: 20, backgroundColor: config.bgColor }}>
                        <View style={{ width: "100%", alignItems: "center", height: "60%", marginVertical: 20, justifyContent: "space-between" }}>
                            <Text>OTP Verification successfully</Text>
                            <Image style={{ width: "100%", height: "75%", resizeMode: "contain" }} source={require("../assets/welcome.png")} />
                            <Text>OTP Verification successfully</Text>
                        </View>
                        <TouchableOpacity style={[styles.loginButton, { alignSelf: "center", width: "80%", height: 45 }]} onPress={() =>
                            this.setState({ WelcomeOverlayVisible: false }, function () {
                                this.props.navigation.navigate("SetMpin")
                            })
                        }>
                            <Text style={{ fontFamily: "Poppins-Regular", color: "white", fontWeight: "600" }}>Let's Set MPIN</Text>
                        </TouchableOpacity>
                    </Overlay>
                </KeyboardAwareScrollView>
            </View >
        )
    }
}
const styles = StyleSheet.create({

    text: {
        textAlign: "center",
        color: "white",
        fontSize: ResponsiveSize(14)
    },
    emailConatiner: {
        width: "100%",
        alignItems: "center",
        justifyContent: "center",
    },
    buttonsContainer: { marginTop: 20, alignSelf: "center" },
    emailOuter: {
        width: "80%",
        backgroundColor: "transparent",
        justifyContent: "center",
        alignItems: "center",
        height: 100,
        alignSelf: "center"
    },
    loginButton: {
        width: "45%",
        backgroundColor: config.themeColor,

        borderRadius: 25,
        justifyContent: "center",
        alignItems: "center",
        height: 45
    },
    codeFieldRoot: {
        //marginTop: 90,
        width: "80%",
        alignSelf: "center"
    },
    cell: {
        width: 50,
        height: 50,
        lineHeight: 50,
        fontSize: ResponsiveSize(config.textSize),
        borderWidth: 2,
        borderRadius: 10,
        borderColor: '#00000030',
        textAlign: 'center'
    },
    focusCell: {
        borderColor: config.themeColor,
    },
    ImageConatiner: {
        width: "100%",
        alignItems: "center",
        justifyContent: "center",
        height: "45%",
    },
    mainContainer: {
        width: "100%",
        flex: 1,
        backgroundColor: config.bgColor
    },
});
const mapStateToProps = state => {
    console.log(state);
    return {
        //  Details: state.detailReducer.DetailList
    };
};

const mapDispatchToProps = (dispatch) => {
    console.log("dispatch====" + JSON.stringify(dispatch));
    return {
        //delete: (key) => dispatch(deleteDetail(key))
        loginDetail: (key) => dispatch(loginDetail(key))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Otp);

