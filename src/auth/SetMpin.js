import { CheckBox, Content } from "native-base";
import React, { Component } from "react";
import { SafeAreaView } from "react-native";
import {
    Text,
    View,
    TouchableOpacity,
    TextInput,
    Image,
    StyleSheet,
    ImageBackground,
    Dimensions, Animated, Keyboard, KeyboardAvoidingView
} from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { ScrollView } from "react-native-gesture-handler";
const ScreenWidth = Dimensions.get("window").width;
const ScreenHeight = Dimensions.get("window").height;
import Toast from 'react-native-tiny-toast';
import { Loader } from "../components/Loader";
import config from "../config/config";
import ResponsiveSize from "../config/ResponsiveSize";
import { loginDetail } from "../actions/Detail";
import { connect } from "react-redux";
import CodeInput from 'react-native-confirmation-code-input';
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { CodeField, Cursor } from "react-native-confirmation-code-field";
import { StatusBar } from "react-native";
const ToastData = {
    position: Toast.position.TOP,
    imgSource: require('../assets/alert.png'),
    imgStyle: { width: 40, height: 40 },
};
class SetMpin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Loader: false,
            Mpin: "",
            ConfirmMpin: "",
            Token: "",
            ShowConfirmCode: false
        };
    }

    storeData = async (key, value) => {
        try {
            await AsyncStorage.setItem(key, value);
            console.log('key==' + key + ', value==' + value);
        } catch (error) {
            // Error saving data
            alert('store data catched');
        }
    };
    componentDidMount() {
        this.retrieveData();
    }
    retrieveData = async key => {
        try {
            const value = await AsyncStorage.getItem('token');
            if (value !== null) {
                // alert (value);
                this.setState({ Token: value, Loader: false }, function () {
                    //this.props.navigation.navigate("Login")
                });
            }
        } catch (error) {
            this.setState({ Loader: false })
            alert('Error retrieving data');
        }
    }
    submit = () => {
        if (this.state.Mpin == '') {
            this.setState({ Loader: false }, function () {
                Toast.show('Please enter mpin', ToastData);
            })
        } else if (this.state.ConfirmMpin == '') {
            this.setState({ Loader: false }, function () {
                Toast.show('Please confirm mpin', ToastData);
            })
        } else if (this.state.Mpin != this.state.ConfirmMpin) {
            this.setState({ Loader: false }, function () {
                Toast.show('Mpin and confirm mpin should be same', ToastData);
            })
        } else {
            const url = config.baseUrl + "student/RegisterMPIN";
            console.log(url);
            console.log("mpin", this.state.Mpin);
            console.log("confirm mpin", this.state.ConfirmMpin);
            fetch(url, {
                method: 'POST',
                headers: {
                    "Accept": 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this.state.Token
                },
                body: JSON.stringify({
                    "mpin": this.state.Mpin
                }),
            })
                .then(response => response.json())
                .then(responseJson => {
                    //alert(JSON.stringify(responseJson))
                    console.log(responseJson);
                    if (responseJson && responseJson.status == true) {
                        this.setState({ Loader: false }, function () {
                            Toast.showSuccess(responseJson.message, { duration: 50, position: Toast.position.CENTER })
                            this.props.navigation.navigate("Login")
                        })
                    } else if (responseJson && responseJson.status == false) {
                        this.setState({ Loader: false }, function () {
                            Toast.show(responseJson.message, ToastData);
                        })
                    } else {
                        this.setState({ Loader: false }, function () {
                            Toast.show(responseJson.title ? responseJson.title : 'Temporary error try again after some time', ToastData);
                        })
                    }
                })
                .catch(error => {
                    this.setState({ Loader: false }, function () {
                        //alert(JSON.stringify(error))
                        console.log(error);
                        Toast.show("error", ToastData);
                    })
                });
        }
    };

    render() {
        return (
            <View style={{ backgroundColor: "white", flex: 1 }}>
                <StatusBar barStyle="light-content" />
                <Loader
                    Loading={this.state.Loader ? true : false}
                />
                <KeyboardAwareScrollView
                    //style={{ backgroundColor: '#4c69a5' }}
                    resetScrollToCoords={{ x: 0, y: 0 }}
                    contentContainerStyle={{ height: ScreenHeight, display: "flex" }}
                    scrollEnabled={true}
                >
                    <View style={styles.mainContainer}>
                        <TouchableOpacity onPress={() => { this.props.navigation.goBack() }} style={{ width: 150, height: 150, alignSelf: "flex-start", position: "absolute", zIndex: 2 }} />
                        <View style={styles.ImageConatiner}>
                            <Image style={{ width: "100%", height: "100%", resizeMode: "contain", alignSelf: "flex-end" }} source={require("../assets/genrateOtp.png")} />
                        </View>
                        <Text style={[styles.text, { color: config.themeColor, alignSelf: "center", textAlign: "center", width: "90%", marginTop: 30, alignSelf: "center" }]}>Enter M-PIN</Text>
                        <View style={styles.emailOuter}>
                            <CodeField
                                {...this.props}
                                value={this.state.Mpin}
                                onChangeText={(text) => this.setState({ Mpin: text }, function () { console.log(this.state.value); })}
                                cellCount={4}
                                blurOnSubmit={true}
                                returnKeyType="done"
                                rootStyle={styles.codeFieldRoot}
                                keyboardType="number-pad"
                                textContentType="oneTimeCode"
                                renderCell={({ index, symbol, isFocused }) => (
                                    <View key={Math.random()} style={{ backgroundColor: "#E9ECFE", borderRadius: 10 }}>
                                        <Text
                                            key={index}
                                            style={[styles.cell, isFocused && styles.focusCell]}
                                        >
                                            {(isFocused ? <Cursor /> : null),
                                                symbol ? "*" : null
                                            }
                                        </Text>
                                    </View>
                                )}
                            />
                        </View>
                        <Text style={[styles.text, { color: config.themeColor, alignSelf: "center", textAlign: "center", width: "90%", marginTop: 30, alignSelf: "center" }]}>Re-Enter M-PIN</Text>
                        <View style={styles.emailOuter}>

                            <CodeField
                                {...this.props}
                                value={this.state.ConfirmMpin}
                                onChangeText={(text) => this.setState({ ConfirmMpin: text }, function () { console.log(this.state.value); })}
                                cellCount={4}
                                returnKeyType="done"
                                blurOnSubmit={true}
                                rootStyle={styles.codeFieldRoot}
                                keyboardType="number-pad"
                                textContentType="oneTimeCode"
                                renderCell={({ index, symbol, isFocused }) => (
                                    <View key={Math.random()} style={{ backgroundColor: "#E9ECFE", borderRadius: 10 }}>
                                        <Text
                                            key={index}
                                            style={[styles.cell, isFocused && styles.focusCell]}
                                        >
                                            {(isFocused ? <Cursor /> : null),
                                                symbol ? "*" : null
                                            }
                                        </Text>
                                    </View>
                                )}
                            />
                        </View>

                        <TouchableOpacity style={[styles.loginButton, { marginTop: 40, alignSelf: "center" }]} onPress={() =>
                            this.setState({ Loader: true }, function () {
                                //this.props.navigation.navigate("ScanInro")
                                this.submit()
                            })
                        }>
                            <Text style={styles.text}>REGISTER</Text>
                        </TouchableOpacity>
                    </View>
                </KeyboardAwareScrollView>
            </View >
        )
    }
}
const styles = StyleSheet.create({

    text: {
        textAlign: "center",
        color: "white",
        fontSize: ResponsiveSize(14)
    },
    emailConatiner: {
        width: "100%",
        alignItems: "center",
        justifyContent: "center",
    },
    buttonsContainer: { marginTop: 20, alignSelf: "center" },
    emailOuter: {
        width: "80%",
        backgroundColor: "transparent",
        justifyContent: "center",
        alignItems: "center",
        height: 80,
        alignSelf: "center"
    },
    loginButton: {
        width: "45%",
        backgroundColor: config.themeColor,

        borderRadius: 25,
        justifyContent: "center",
        alignItems: "center",
        height: 45
    },
    codeFieldRoot: {
        //marginTop: 90,
        width: "80%",
        alignSelf: "center"
    },
    cell: {
        width: 50,
        height: 50,
        lineHeight: 50,
        fontSize: ResponsiveSize(config.textSize),
        borderWidth: 2,
        borderRadius: 10,
        borderColor: '#00000030',
        textAlign: 'center'
    },
    focusCell: {
        borderColor: config.themeColor,
    },
    ImageConatiner: {
        width: "100%",
        alignItems: "center",
        height: "25%",
    },
    mainContainer: {
        width: "100%",
        flex: 1,
        backgroundColor: config.bgColor
    },
});
const mapStateToProps = state => {
    console.log(state);
    return {
        //  Details: state.detailReducer.DetailList
    };
};

const mapDispatchToProps = (dispatch) => {
    //console.log("dispatch====" + dispatch);
    return {
        //delete: (key) => dispatch(deleteDetail(key))
        loginDetail: (key) => dispatch(loginDetail(key))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SetMpin);
