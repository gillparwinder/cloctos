import React, { Component } from "react";
import { ImageBackground } from "react-native";
import { Dimensions, TextInput, TouchableOpacity, KeyboardAvoidingView, View, Text, SafeAreaView, Image } from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import { SimpleAnimation } from 'react-native-simple-animations';
import config from "../config/config";
const ScreenHeight = Dimensions.get("window").height;
const ScreenWidth = Dimensions.get("window").width;
export default class BottomTab extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Home: true, active: ""
        }
    }

    render() {
        const { navigation, active, onPress } = this.props;
        return (
            <View style={{ width: "100%", backgroundColor: "transparent", alignSelf: "center", height: 100, borderRadius: 20, flexDirection: "row", alignItems: "center", justifyContent: "space-between", zIndex: 2 }}>
                {active == "home" ?
                    //<SimpleAnimation style={{ width: "100%", height: "100%", resizeMode: "contain", position: "absolute" }} delay={500} duration={1000} fade staticType='zoom'>
                    <Image style={{ width: "100%", height: "100%", resizeMode: "contain", position: "absolute" }} source={require("../assets/tab1.png")} />
                    //</SimpleAnimation>
                    : active == "subject" ?
                        //<SimpleAnimation style={{ width: "100%", height: "100%", resizeMode: "contain", position: "absolute" }} delay={500} duration={1000} fade staticType='zoom'>
                        <Image style={{ width: "100%", height: "100%", resizeMode: "contain", position: "absolute" }} source={require("../assets/tab2.png")} />
                        //</SimpleAnimation>
                        : active == "Time Table" ?
                            // <SimpleAnimation style={{ width: "100%", height: "100%", resizeMode: "contain", position: "absolute" }} delay={500} duration={1000} fade staticType='zoom'>
                            <Image style={{ width: "100%", height: "100%", resizeMode: "contain", position: "absolute" }} source={require("../assets/tab3.png")} />
                            //</SimpleAnimation>
                            : active == "result" ?
                                // <SimpleAnimation style={{ width: "100%", height: "100%", resizeMode: "contain", position: "absolute" }} delay={500} duration={1000} fade staticType='zoom'>
                                <Image style={{ width: "100%", height: "100%", resizeMode: "contain", position: "absolute" }} source={require("../assets/tab4.png")} />
                                //</SimpleAnimation>
                                :
                                //<SimpleAnimation style={{ width: "100%", height: "100%", resizeMode: "contain", position: "absolute" }} delay={500} duration={1000} fade staticType='zoom'>
                                <Image style={{ width: "100%", height: "100%", resizeMode: "contain", position: "absolute" }} source={require("../assets/tab1.png")} />
                    //</SimpleAnimation>
                }
                <TouchableOpacity onPress={() => {
                    onPress("home")
                    //navigation.navigate("DetailScreen")
                }} style={{ width: "25%", height: "100%", justifyContent: "center", alignItems: "center" }}>
                    {/*<View style={{ width: 40, height: 40, marginTop: -20, justifyContent: "center", alignItems: "center", backgroundColor: "white", borderRadius: 50 }}>
                        <View style={{ width: 15, height: 15, backgroundColor: "#23C4D7", borderRadius: 20 }}></View>
                    </View>*/}
                    {/*{active == "home" ?
                        <Image style={{ width: 50, height: 50, resizeMode: "contain" }} source={require("../assets/homeFilled.png")} />
                        :
                        <Image style={{ width: 50, height: 50, resizeMode: "contain" }} source={require("../assets/home.png")} />
                    }*/}
                </TouchableOpacity>
                <TouchableOpacity onPress={() => {
                    onPress("subject")
                    //navigation.navigate("DetailScreen")
                }} style={{ width: "25%", height: "100%", justifyContent: "center", alignItems: "center" }}>
                    {/*{active == "subject" ?
                        <Image style={{ width: 50, height: 50, resizeMode: "contain" }} source={require("../assets/subjectFilled.png")} />
                        :
                        <Image style={{ width: 50, height: 50, resizeMode: "contain" }} source={require("../assets/subject.png")} />
                    }*/}
                </TouchableOpacity>
                <TouchableOpacity onPress={() => {
                    onPress("Time Table")
                    //navigation.navigate("DetailScreen")
                }} style={{ width: "25%", height: "100%", justifyContent: "center", alignItems: "center" }}>
                    {/*{active == "schedule" ?
                        <Image style={{ width: 50, height: 50, resizeMode: "contain" }} source={require("../assets/scheduleFilled.png")} />
                        :
                        <Image style={{ width: 50, height: 50, resizeMode: "contain" }} source={require("../assets/schedule.png")} />
                    }*/}
                </TouchableOpacity>
                <TouchableOpacity onPress={() => {
                    onPress("result")
                    //navigation.navigate("DetailScreen")
                }} style={{ width: "25%", height: "100%", justifyContent: "center", alignItems: "center" }}>
                    {/*{active == "result" ?
                        <Image style={{ width: 50, height: 50, resizeMode: "contain" }} source={require("../assets/resultFilled.png")} />
                        :
                        <Image style={{ width: 50, height: 50, resizeMode: "contain" }} source={require("../assets/result.png")} />
                    }*/}
                </TouchableOpacity>
            </View>
        )
    }
}