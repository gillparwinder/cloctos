import React, { Component } from "react";
import { Image, TouchableOpacity, SafeAreaView, View, Text, Dimensions, ScrollView, StyleSheet } from "react-native";
import { CustomHeader } from "./CustomHeader";
import { Loader } from "./Loader";
import { SimpleAnimation } from 'react-native-simple-animations';
import { Fab } from "native-base";
import config from "../config/config";
import Toast from 'react-native-tiny-toast';
import ResponsiveSize from "../config/ResponsiveSize";
import { FlatList } from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import BottomTab from "./BottomTab";
import DetailScreen from "./DetailScreen";
import HomeScreen from "./HomeScreen";
import SubjectList from "./SubjectList";
import TimeTable from "./TimeTable";
import { StatusBar } from "react-native";
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
const ToastData = {
    position: Toast.position.TOP,
    imgSource: require('../assets/alert.png'),
    imgStyle: { width: 40, height: 40 },
};
export default class BottomTabHomeScreen extends Component {
    constructor() {
        super();
        this.state = {
            Active: "home"

        }
    }
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: "white", justifyContent: "flex-end", paddingBottom: 10 }}>
                <StatusBar barStyle="light-content" />
                <SafeAreaView style={{ flex: 1, justifyContent: "space-between" }}>
                    <CustomHeader
                        isHome={true}
                        title={this.state.Active}
                        notification={true}
                        navigation={this.props.navigation}
                    />


                    {this.state.Active == "home" ?
                        <HomeScreen {...this.props}>{"home"}</HomeScreen>
                        : null}
                    {this.state.Active == "subject" ?

                        <SubjectList {...this.props} >{"Subjects"}</SubjectList>
                        //<DetailScreen {...this.props} >{"Subjects"}</DetailScreen>
                        : null}
                    {this.state.Active == "Time Table" ?
                        <TimeTable {...this.props}>{"Time Table"}</TimeTable>
                        //<DetailScreen {...this.props}>{"Time Table"}</DetailScreen>
                        : null}
                    {this.state.Active == "result" ?
                        <DetailScreen {...this.props}>{"Performance"}</DetailScreen>
                        : null}

                </SafeAreaView>
                <View style={{ width: "100%", height: 120, backgroundColor: "transparent", position: "absolute", marginBottom: 10 }}>
                    <BottomTab
                        active={this.state.Active == "home" ? "HomeScreen" : this.state.Active}
                        onPress={(props) => {
                            this.setState({ Active: props })
                        }}
                        navigation={this.props.navigation}
                    />
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    FlatlistChildView: {
        width: "45%",
        height: 100,
        borderRadius: 10,
        flexDirection: 'column',
        backgroundColor: "#383E7B",
        margin: 10,
        justifyContent: "center",
        alignItems: "center"
    },
    ChildViewText: {
        fontSize: ResponsiveSize(16),
        color: "white",
        fontWeight: "500"
    },
    ChildViewImage: {
        justifyContent: 'center',
        alignItems: 'center',
        height: "100%",
        width: "100%",
        position: "absolute",
        borderRadius: 10,
    },
})