import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { Component } from "react";
import { Image } from "react-native";
import { SafeAreaView, StyleSheet, Switch, ScrollView, } from "react-native";
import { View, Text } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { CustomHeader } from "./CustomHeader";
import { Loader } from "./Loader";
import Collapsible from 'react-native-collapsible';
import Accordion from 'react-native-collapsible/Accordion';
import DatePicker from 'react-native-datepicker'
import * as Animatable from 'react-native-animatable';
import Toast from 'react-native-tiny-toast';
import ResponsiveSize from "../config/ResponsiveSize";
import config from "../config/config";
import moment from "moment";
import { color, log } from "react-native-reanimated";
import { ImageBackground } from "react-native";
const ToastData = {
    position: Toast.position.TOP,
    imgSource: require('../assets/alert.png'),
    imgStyle: { width: 40, height: 40 },
};
const BACON_IPSUM =
    'Bacon ipsum dolor amet chuck turducken landjaeger tongue spare ribs. Picanha beef prosciutto meatball turkey shoulder shank salami cupim doner jowl pork belly cow. Chicken shankle rump swine tail frankfurter meatloaf ground round flank ham hock tongue shank andouille boudin brisket. ';

const CONTENT = [
    {
        title: 'ENGLISH',
        content: BACON_IPSUM,
    },
    {
        title: 'MATHS',
        content: BACON_IPSUM,
    },
    {
        title: 'SCIENCE',
        content: BACON_IPSUM,
    },
    {
        title: 'HINDI',
        content: BACON_IPSUM,
    },
    {
        title: 'PUNJABI',
        content: BACON_IPSUM,
    },
];
const GreenColor = "#23C4D7";
const BlueColor = "#6397F2";
const LightBlueColor = "#E0EBFF";
const PurpleColor = "#7260E9";
const LightPurpleColor = "#A99CFF";
export default class DetailScreenMenu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Loader: true,
            activeSections: [],
            activeSections2: [],
            collapsed: true,
            multipleSelect: false,
            ApiRequestRequired: true,
            TimeTable: [],
            TempItem: "",
            Subjects: this.props.route.params ? (this.props.route.params.Subjects ? this.props.route.params.Subjects : {}) : {},
            TempData: ["Exam Wise", "Subject Wise"],
            ProfileData: {},
            colorCode: this.props.route.params ? (this.props.route.params.colorCode ? this.props.route.params.colorCode : "") : "",
            colorPic: this.props.route.params ? (this.props.route.params.colorPic ? this.props.route.params.colorPic : "") : "",
            CamTimeTable: [],
            Attendance: [],
            Performance: [],
            SubjectsDetail: [],
            InternalPerformance: [],
            UniversityTimeTable: [],
            InternalMarks: [],
            UniversityMarks: [],
            Colors: [
                "white",
                "#BBECF2",
                "white",
                "#BBECF2",
                "white",
                "#BBECF2",
                "white",
                "#BBECF2",
                "white",
                "#BBECF2",
                "white",
                "#BBECF2",
                "white",
                "#BBECF2",
                "white",
                "#BBECF2",
                "white",
                "#BBECF2",
                "white",
                "#BBECF2",
            ],
            BlueColors: [
                "white",
                "#E0EBFF",
                "white",
                "#E0EBFF",
                "white",
                "#E0EBFF",
                "white",
                "#E0EBFF",
                "white",
                "#E0EBFF",
                "white",
                "#E0EBFF",
                "white",
                "#E0EBFF",
                "white",
                "#E0EBFF",
                "white",
                "#E0EBFF",
                "white",
                "#E0EBFF",
            ],
            PurpleColors: [
                "#E9E6FE",
                "white",
                "#E9E6FE",
                "white",
                "#E9E6FE",
                "white",
                "#E9E6FE",
                "white",
                "#E9E6FE",
                "white",
                "#E9E6FE",
                "white",
                "#E9E6FE",
                "white",
                "#E9E6FE",
                "white",
                "#E9E6FE",
                "white",
                "#E9E6FE",
                "white",
            ],
            TempIndex: -1,
            filterredSubTempIndex: -1,
            AttendanceStartDate: "",
            AttendanceEndDate: "",
            filterredUnitNameTempTpocName: "",
            CurrentDate: new Date(),
            TempTimeTable: this.props.route.params ? (this.props.route.params.TimeTable ? this.props.route.params.TimeTable : {}) : {},
            Title: this.props.children ? this.props.children : ""
            //Token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI2MzE1MmMxYS02YTU1LTRkMDktODE1ZC1hYTk1ZDhhZjgyZDkiLCJBcHBObyI6IjIyNDQ3OSIsIklzU2Nob29sIjoidHJ1ZSIsIkRCIjoiYjdCUTExWlpyV2tHVEl0OVdqbGFURURaM0l2cHZxZVZJSElvMStBMVY5dSpLdzdtOXNCZFNoMm55MzNBdlBSU083QUFndE9pQU5LZjlyVTE4WEI3bGQrdmpHV1lUZSpNQVVVMFE4WHF1K1VhVzdKUDlPQm9GQUdnN3d4a2liWFk4YmM5ZFhNd3NuckRxK1JjenpJamlrMnQrbFlhN3dCZE5HUkgrWUl4aU56T3B5dzJGUmU4MTV3YTI4VzNLTEEwRTcrWVpwRHA2Q0hRUlFCdXFrVTVZcDFYMktMeU15QlpEWFdrVEIxcEZIc3BTZklYR1UyMGYqeGpNRTAzdGhKSThVZVVXRHk3Vnh6VmVMZUpBY1A0eHJSK0RxT2d0Kk1QZjBHSUpCemVZdm11NTRjc0gzU29ubEY1SWFaVVNVZ2orN25FRktaMHpkS3V4NVhLZUFEWktBPT0iLCJCYXRjaFllYXIiOiIyMDE4IiwiQ291cnNlTmFtZSI6IkIuQSIsIkN1cnJlbnRTZW1lc3RlciI6IjYiLCJDdXJyZW50WWVhciI6IiIsIkRlZ3JlZUNvZGUiOiI4NiIsIlJvbGxObyI6IjExOEVHMDEzNTIiLCJOYW1lIjoiQUdBTFlBIEQuIiwibmJmIjoxNjE0MjM5NjcyLCJleHAiOjE2NDU3NzU2NzIsImlzcyI6InBhbHBhcC1wcm9kLWVudmlyb25tZW50IiwiYXVkIjoicGFscGFwcHJvZHVzZXJzIn0.vDA0Ccf3vX7xiYNGIUIP6zyi7RAWUsgG9N4IzF6M2Uk"

        }
    }
    componentDidMount() {
        console.log("props----", this.props.route.params);
        //this.timeout = setTimeout(() => {
        //    this.setState({ Loader: false })
        //}, 2000);
        this.retrieveData();

    }
    retrieveData = async key => {
        try {
            const value = await AsyncStorage.getItem('token');
            if (value !== null) {
                // alert (value);
                this.setState({ Token: value }, function () {
                    if (this.state.Title == "Student Profile") {
                        this.onLoadGetProfile()
                        //this.setState({ Loader: false })
                    } else if (this.state.Title == "Subjects") {
                        this.onPressGetSubjectsDetail()

                    } else if (this.state.Title == "Time Table") {
                        this.state.TempTimeTable.day == "Today" ?
                            this.onLoadGetTodayTimeTable()
                            :
                            this.state.TempTimeTable.day == "Weekly" ?
                                this.onLoadGetTimeTable()
                                :
                                this.state.TempTimeTable.day == "CAM" ?
                                    this.onLoadCamExamTimeTable()
                                    :
                                    this.state.TempTimeTable.day == "University" ?
                                        this.onLoadUniversityTimeTable()
                                        : null

                    } else if (this.state.Title == "Attendance") {
                        //this.setState({ Loader: false })
                        let date = new Date();
                        let finalCurrentDate = moment(date).format("YYYY-MM-DD");
                        let newdate = date.setDate(date.getDate() - 7);
                        finalDate = moment(newdate).format("YYYY-MM-DD");
                        this.setState({ AttendanceEndDate: finalCurrentDate, AttendanceStartDate: finalDate }, function () {
                            this.onPressGetAttendance()
                        })
                    } else if (this.state.Title == "Internal Marks") {
                        this.onLoadGetInternalMarks()
                    } else if (this.state.Title == "University Marks") {
                        this.onLoadGetUniversitylMarks()
                    } else if (this.state.Title == "Performance") {
                        this.onLoadGetPerformance()
                    } else {
                        this.setState({ Loader: false })
                    }
                });
            }
        } catch (error) {
            alert('Error retrieving data');
        }
    }
    onLoadGetProfile = () => {
        const url = config.baseUrl + "student/Profile";
        console.log(url);
        fetch(url, {
            method: 'GET',
            headers: {
                "Accept": 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.state.Token
            },
            //body: JSON.stringify({
            //    "mpin": this.state.Mpin
            //}),
        })
            .then(response => response.json())
            .then(responseJson => {
                //alert(JSON.stringify(responseJson))
                console.log(responseJson);
                if (responseJson) {

                    this.setState({ Loader: false, ProfileData: responseJson }, function () {

                    })

                } else {
                    this.setState({ Loader: false }, function () {
                        Toast.show(responseJson.title ? responseJson.title : 'Temporary error try again after some time', ToastData);
                    })
                }
            })
            .catch(error => {
                this.setState({ Loader: false }, function () {
                    //alert(JSON.stringify(error))
                    console.log(error);
                    Toast.show("error", ToastData);
                })
            });
    }
    onLoadGetInternalPerformance = () => {
        const url = config.baseUrl + "student/StudentSubjectWiseInternalMarks";
        console.log(url);
        fetch(url, {
            method: 'GET',
            headers: {
                "Accept": 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.state.Token
            },
            //body: JSON.stringify({
            //    "mpin": this.state.Mpin
            //}),
        })
            .then(response => response.json())
            .then(responseJson => {
                //alert(JSON.stringify(responseJson))
                console.log("responseJson.semesterDetails====", responseJson.semesterDetails);
                if (responseJson) {

                    this.setState({ Loader: false, InternalPerformance: responseJson.semesterDetails }, function () {

                    })

                } else {
                    this.setState({ Loader: false }, function () {
                        Toast.show(responseJson.title ? responseJson.title : 'Temporary error try again after some time', ToastData);
                    })
                }
            })
            .catch(error => {
                this.setState({ Loader: false }, function () {
                    //alert(JSON.stringify(error))
                    console.log(error);
                    Toast.show("error", ToastData);
                })
            });
    }
    onLoadGetPerformance = () => {
        const url = config.baseUrl + "student/StudentSubjectWisePerformance";
        console.log(url);
        fetch(url, {
            method: 'GET',
            headers: {
                "Accept": 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.state.Token
            },
            //body: JSON.stringify({
            //    "mpin": this.state.Mpin
            //}),
        })
            .then(response => response.json())
            .then(responseJson => {
                //alert(JSON.stringify(responseJson))
                console.log(responseJson);
                if (responseJson) {

                    this.setState({ Loader: false, Performance: responseJson.semesterWiseDetails }, function () {

                    })

                } else {
                    this.setState({ Loader: false }, function () {
                        Toast.show(responseJson.title ? responseJson.title : 'Temporary error try again after some time', ToastData);
                    })
                }
            })
            .catch(error => {
                this.setState({ Loader: false }, function () {
                    //alert(JSON.stringify(error))
                    console.log(error);
                    Toast.show("error", ToastData);
                })
            });
    }
    onLoadGetInternalMarks = () => {
        const url = config.baseUrl + "student/InternalMarks";
        console.log(url);
        fetch(url, {
            method: 'GET',
            headers: {
                "Accept": 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.state.Token
            },
            //body: JSON.stringify({
            //    "mpin": this.state.Mpin
            //}),
        })
            .then(response => response.json())
            .then(responseJson => {
                this.onLoadGetInternalPerformance()
                //alert(JSON.stringify(responseJson))
                console.log(responseJson);
                if (responseJson) {

                    this.setState({ InternalMarks: responseJson }, function () {

                    })

                } else {
                    this.setState({ Loader: false }, function () {
                        Toast.show(responseJson.title ? responseJson.title : 'Temporary error try again after some time', ToastData);
                    })
                }
            })
            .catch(error => {
                this.onLoadGetInternalPerformance()
                this.setState({}, function () {
                    //alert(JSON.stringify(error))
                    console.log(error);
                    Toast.show("error", ToastData);
                })
            });
    }
    onLoadGetUniversitylMarks = () => {
        const url = config.baseUrl + "student/UniversityMarks";
        console.log(url);
        fetch(url, {
            method: 'GET',
            headers: {
                "Accept": 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.state.Token
            },
            //body: JSON.stringify({
            //    "mpin": this.state.Mpin
            //}),
        })
            .then(response => response.json())
            .then(responseJson => {
                //alert(JSON.stringify(responseJson))
                console.log(responseJson);
                if (responseJson) {

                    this.setState({ Loader: false, UniversityMarks: responseJson }, function () {

                    })

                } else {
                    this.setState({ Loader: false }, function () {
                        Toast.show(responseJson.title ? responseJson.title : 'Temporary error try again after some time', ToastData);
                    })
                }
            })
            .catch(error => {
                this.setState({ Loader: false }, function () {
                    //alert(JSON.stringify(error))
                    console.log(error);
                    Toast.show("error", ToastData);
                })
            });
    }
    onPressGetAttendance = () => {
        const url = config.baseUrl + "student/Attendance";
        console.log(url);
        fetch(url, {
            method: 'POST',
            headers: {
                "Accept": 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.state.Token
            },
            body: JSON.stringify({
                "fromDate": this.state.AttendanceStartDate,
                "toDate": this.state.AttendanceEndDate
            }),
        })
            .then(response => response.json())
            .then(responseJson => {
                //alert(JSON.stringify(responseJson))
                console.log(responseJson);
                if (responseJson) {
                    //let date = new Date();
                    //let finalCurrentDate = moment(date).format("DD-MM-YYYY");
                    //let newdate = date.setDate(date.getDate() - 7);
                    //finalDate = moment(newdate).format("DD-MM-YYYY")
                    //this.setState({ Loader: false, Attendance: responseJson, AttendanceStartDate: finalDate, AttendanceEndDate: finalCurrentDate, }, function () {
                    this.setState({ Loader: false, AttendanceData: responseJson, Attendance: responseJson.studentAttendance, ApiRequestRequired: false }, function () {
                    })

                } else {
                    this.setState({ Loader: false }, function () {
                        Toast.show(responseJson.title ? responseJson.title : 'Temporary error try again after some time', ToastData);
                    })
                }
            })
            .catch(error => {
                this.setState({ Loader: false }, function () {
                    //alert(JSON.stringify(error))
                    console.log(error);
                    Toast.show("error", ToastData);
                })
            });
    }
    //onLoadGetSubjects = () => {
    //    const url = config.baseUrl + "student/StudentSubjectList";
    //    console.log(url);
    //    fetch(url, {
    //        method: 'GET',
    //        headers: {
    //            "Accept": 'application/json',
    //            'Content-Type': 'application/json',
    //            'Authorization': 'Bearer ' + this.state.Token
    //        },
    //        //body: JSON.stringify({
    //        //    "mpin": this.state.Mpin
    //        //}),
    //    })
    //        .then(response => response.json())
    //        .then(responseJson => {
    //            //alert(JSON.stringify(responseJson))
    //            console.log(responseJson);
    //            if (responseJson) {

    //                this.setState({ Loader: false, Subjects: responseJson }, function () {

    //                })

    //            } else {
    //                this.setState({ Loader: false }, function () {
    //                    Toast.show(responseJson.title ? responseJson.title : 'Temporary error try again after some time', ToastData);
    //                })
    //            }
    //        })
    //        .catch(error => {
    //            this.setState({ Loader: false }, function () {
    //                //alert(JSON.stringify(error))
    //                console.log(error);
    //                Toast.show("error", ToastData);
    //            })
    //        });
    //}
    onPressGetSubjectsDetail = () => {
        const url = config.baseUrl + "student/SubjectSyllabusDetails";
        console.log(url);
        fetch(url, {
            method: 'GET',
            headers: {
                "Accept": 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.state.Token
            },
            //body: JSON.stringify({
            //    "mpin": this.state.Mpin
            //}),
        })
            .then(response => response.json())
            .then(responseJson => {
                //alert(JSON.stringify(responseJson))
                console.log(responseJson);
                if (responseJson) {

                    this.setState({ Loader: false, SubjectsDetail: responseJson }, function () {
                        //this.onLoadGetSubjects()
                    })

                } else {
                    this.setState({ Loader: false }, function () {
                        Toast.show(responseJson.title ? responseJson.title : 'Temporary error try again after some time', ToastData);
                    })
                }
            })
            .catch(error => {
                this.setState({ Loader: false }, function () {
                    //alert(JSON.stringify(error))
                    console.log(error);
                    Toast.show("error", ToastData);
                })
            });
    }
    onLoadGetTimeTable = () => {
        const url = config.baseUrl + "student/WeekTimeTable";
        console.log(url);
        fetch(url, {
            method: 'GET',
            headers: {
                "Accept": 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.state.Token
            },
            //body: JSON.stringify({
            //    "mpin": this.state.Mpin
            //}),
        })
            .then(response => response.json())
            .then(responseJson => {
                //alert(JSON.stringify(responseJson))
                //this.onLoadCamExamTimeTable()
                console.log(responseJson);
                if (responseJson) {

                    this.setState({ Loader: false, TimeTable: responseJson, TempItem: responseJson[0], TimeTableTempIndex: 0 }, function () {
                        //this.setState({ Loader: false, TimeTable: responseJson }, function () {

                    })
                } else {
                    this.setState({}, function () {
                        this.setState({ Loader: false }, function () {
                            Toast.show(responseJson.title ? responseJson.title : 'Temporary error try again after some time', ToastData);
                        })
                    })
                }
            })
            .catch(error => {
                this.setState({}, function () {
                    this.setState({ Loader: false }, function () {
                        //alert(JSON.stringify(error))
                        console.log(error);
                        //this.onLoadCamExamTimeTable()
                        Toast.show("error", ToastData);
                    })
                })
            });
    }
    onLoadCamExamTimeTable = () => {
        const url = config.baseUrl + "student/CamTimeTable";
        console.log(url);
        fetch(url, {
            method: 'GET',
            headers: {
                "Accept": 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.state.Token
            },
            //body: JSON.stringify({
            //    "mpin": this.state.Mpin
            //}),
        })
            .then(response => response.json())
            .then(responseJson => {
                //alert(JSON.stringify(responseJson))
                //this.onLoadUniversityTimeTable()
                console.log(responseJson);
                if (responseJson) {

                    this.setState({ Loader: false, CamTimeTable: responseJson }, function () {
                        //this.setState({ Loader: false, CamTimeTable: responseJson }, function () {

                    })
                } else {
                    this.setState({}, function () {
                        this.setState({ Loader: false }, function () {
                            Toast.show(responseJson.title ? responseJson.title : 'Temporary error try again after some time', ToastData);
                        })
                    })
                }
            })
            .catch(error => {
                this.setState({ Loader: false }, function () {
                    //this.setState({ Loader: false }, function () {
                    //alert(JSON.stringify(error))
                    console.log(error);
                    //this.onLoadUniversityTimeTable()
                    Toast.show("error", ToastData);
                })
            });
    }
    onLoadUniversityTimeTable = () => {
        const url = config.baseUrl + "student/UniversityTimeTableDetails";
        console.log(url);
        fetch(url, {
            method: 'GET',
            headers: {
                "Accept": 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.state.Token
            },
            //body: JSON.stringify({
            //    "mpin": this.state.Mpin
            //}),
        })
            .then(response => response.json())
            .then(responseJson => {
                //alert(JSON.stringify(responseJson))
                console.log(responseJson);
                if (responseJson) {

                    this.setState({ Loader: false, UniversityTimeTable: responseJson }, function () {

                    })
                } else {
                    this.setState({ Loader: false }, function () {
                        Toast.show(responseJson.title ? responseJson.title : 'Temporary error try again after some time', ToastData);
                    })
                }
            })
            .catch(error => {
                this.setState({ Loader: false }, function () {
                    //alert(JSON.stringify(error))
                    console.log(error);
                    Toast.show("error", ToastData);
                })
            });
    }
    onLoadGetTodayTimeTable = () => {
        const url = config.baseUrl + "student/StudentClassDetails";
        console.log(url);
        fetch(url, {
            method: 'GET',
            headers: {
                "Accept": 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.state.Token
            },
            //body: JSON.stringify({
            //    "mpin": this.state.Mpin
            //}),
        })
            .then(response => response.json())
            .then(responseJson => {
                //alert(JSON.stringify(responseJson))
                //this.onLoadGetTimeTable()
                console.log(responseJson);
                if (responseJson) {

                    this.setState({ Loader: false, TodayTimeTable: responseJson }, function () {
                        //this.setState({ Loader: false, TodayTimeTable: responseJson }, function () {

                    })
                } else {
                    this.setState({}, function () {
                        this.setState({ Loader: false }, function () {
                            Toast.show(responseJson.title ? responseJson.title : 'Temporary error try again after some time', ToastData);
                        })
                    })
                }
            })
            .catch(error => {
                this.setState({}, function () {
                    this.setState({ Loader: false }, function () {
                        //alert(JSON.stringify(error))
                        //this.onLoadGetTimeTable()
                        console.log(error);
                        Toast.show("error", ToastData);
                    })
                })
            });
    }




    setSections = sections => {
        this.setState({
            activeSections: sections.includes(undefined) ? [] : sections,
        }, function () {
            this.onPressGetSubjectsDetail()
        });
    };
    setSections2 = sections => {
        this.setState({
            activeSections2: sections.includes(undefined) ? [] : sections,
        }, function () {
            //this.onPressGetSubjectsDetail()
        });
    };

    renderHeader = (section, _, isActive) => {
        return (
            <Animatable.View
                duration={400}
                style={[styles.header, isActive ? styles.active : styles.inactive]}
                transition="backgroundColor"
            >
                <Text style={styles.headerText}>{section.subjectName}</Text>
            </Animatable.View>
        );
    };
    renderHeader2 = () => {

        return (<View style={styles.SubjectMainContainer}>
            <Image style={styles.SubjectNameImageView} source={require('../assets/blueBg.png')}>
            </Image>
            <View style={styles.SubjectNameTextView}>
                <Text numberOfLines={1} style={styles.SubjectNameTextStyle}>{this.state.TempTimeTable.day}</Text>
            </View>
            <Animatable.View key={Math.random()}
                duration={400}
                style={{ width: "100%", alignItems: "center" }}
                transition="backgroundColor"
            >
                <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                    {this.state.TimeTable.length ? this.state.TimeTable.map((item, index) => {
                        return <TouchableOpacity activeOpacity={0.6} style={{ margin: 10, elevation: 3, shadowColor: "silver", shadowOpacity: .6, shadowOffset: { width: 1, height: 1 }, borderRadius: 15, width: 70, height: 70, backgroundColor: this.state.TimeTableTempIndex == index ? BlueColor : "white", justifyContent: "center", alignItems: "center" }} onPress={() => {
                            if (this.state.TimeTableTempIndex == index) {
                                this.setState({ TimeTableTempIndex: -1, TempItem: "" })
                            } else {
                                //console.log("item====", item);
                                this.setState({ TimeTableTempIndex: index, TempItem: item })
                            }
                        }}>
                            <Text style={{ fontFamily: "Poppins-Regular", color: this.state.TimeTableTempIndex == index ? "white" : BlueColor, fontSize: ResponsiveSize(14), fontWeight: "600", textTransform: "uppercase" }}>{item.day}</Text>

                        </TouchableOpacity>



                    })
                        : !this.state.Loader ?
                            <Animatable.View key={Math.random()}
                                duration={400}
                                style={[styles.header, { width: "100%", alignSelf: "center" }]}
                                transition="backgroundColor"
                            >
                                <Text style={[styles.headerText, { textTransform: "none" }]}>Oops! No data avaiblabe</Text>
                            </Animatable.View>
                            : null}
                </ScrollView>
                {this.state.TempItem != "" ?
                    this.renderContent2(this.state.TempItem)
                    : null}
            </Animatable.View>
        </View>
        )
    };
    renderCamTestDetailsContent = (testDetails, internalTestName) => {
        let CamExamArr = testDetails;
        var filterred = CamExamArr.filter(function (item) {
            console.log(item.internalTestName == internalTestName);
            return item.internalTestName == internalTestName;
        }).map(function ({ internalTestName, staffCode, examDate, duration, startTime, endTime }) {
            return { internalTestName, staffCode, examDate, duration, startTime, endTime };
        });
        console.log("filterredCamContent==========", filterred);
        return (
            filterred.length ? filterred.map((item, index) => {
                return <Animatable.View key={Math.random()}
                    duration={400}
                    style={{ width: "95%", alignSelf: "center", padding: 10, minHeight: 70, backgroundColor: "white", elevation: 3, shadowColor: "silver", shadowOpacity: .6, shadowOffset: { width: 1, height: 1 }, marginTop: 10, borderRadius: 5 }}
                    transition="backgroundColor"
                >
                    <View activeOpacity={0.6} style={{ justifyContent: "center", alignItems: "center", flexDirection: "row", width: "100%", alignItems: "center", flexDirection: "row" }} >
                        <View style={{ width: "10%", minHeight: 80, justifyContent: "center", alignItems: "center" }}>
                            <Text style={{ fontFamily: "Poppins-Regular", color: PurpleColor, fontWeight: "600", fontSize: ResponsiveSize(14) }}>|</Text>
                        </View>
                        <View style={{ width: "50%", justifyContent: "center" }}>
                            {item.internalTestName ? <Text style={[styles.headerText, { textTransform: "none", color: PurpleColor, fontSize: ResponsiveSize(14) }]}>{item.internalTestName}</Text> : null}
                            <View style={{ width: "100%", alignItems: "center", flexDirection: "row", marginTop: 5 }}>
                                {item.startTime ? <Text style={[styles.headerText, { textTransform: "none", color: LightPurpleColor }]}>{item.startTime}</Text> : null}
                                {item.startTime ? <Text style={[styles.headerText, { textTransform: "none", color: LightPurpleColor }]}> to </Text> : null}
                                {item.endTime ? <Text style={[styles.headerText, { textTransform: "none", color: LightPurpleColor }]}>{item.endTime}</Text> : null}
                            </View>
                        </View>
                        <View style={{ width: "40%", justifyContent: "center", paddingHorizontal: 5 }}>
                            {item.examDate ? <Text style={[styles.headerText, { textTransform: "none" }]}>{item.examDate}</Text> : null}
                            {item.duration ? <Text style={[styles.headerText, { textTransform: "none", marginTop: 5 }]}>{item.duration}</Text> : null}
                            {/*{item.subjectCode ? <Text style={[styles.headerText, { textTransform: "none", marginTop: 5 }]}>{item.subjectCode}</Text> : null}*/}
                            {item.staffCode ? <Text style={[styles.headerText, { textTransform: "none", marginTop: 5 }]}>{item.staffCode}</Text> : null}
                        </View>
                        {/*<View style={{ width: "100%", paddingLeft: 10, justifyContent: "center", }}>
                            {item.internalTestName ? <Text style={[styles.headerText, { textTransform: "none" }]}>Internal Test Name : {item.internalTestName}</Text> : null}
                            {item.subjectCode ? <Text style={[styles.headerText, { textTransform: "none" }]}>Subject Code : {item.subjectCode}</Text> : null}
                            {item.staffCode ? <Text style={[styles.headerText, { textTransform: "none" }]}>Staff Code : {item.staffCode}</Text> : null}
                            {item.examDate ? <Text style={[styles.headerText, { textTransform: "none" }]}>Exam Date : {item.examDate}</Text> : null}
                            {item.duration ? <Text style={[styles.headerText, { textTransform: "none" }]}>Duration : {item.duration}</Text> : null}
                            {item.startTime ? <Text style={[styles.headerText, { textTransform: "none" }]}>Start Time : {item.startTime}</Text> : null}
                            {item.endTime ? <Text style={[styles.headerText, { textTransform: "none" }]}>End Time : {item.endTime}</Text> : null}
                        </View>*/}

                    </View>
                </Animatable.View>

            })
                : !this.state.Loader ?
                    <Animatable.View key={Math.random()}
                        duration={400}
                        style={[styles.header, { width: "100%", alignSelf: "center" }]}
                        transition="backgroundColor"
                    >
                        <Text style={[styles.headerText, { textTransform: "none" }]}>Oops! No data avaiblabe</Text>
                    </Animatable.View>
                    : null
        );
    };

    renderPerformanceHeader = () => {
        let PerformanceArr = this.state.Performance;
        return PerformanceArr.length ? PerformanceArr.map((item, index) => {
            return <Animatable.View key={Math.random()}
                duration={400}
                style={[styles.header, { width: "97%", alignSelf: "center", backgroundColor: this.state.PerformanceHeaderTempIndex == index ? "#DCE8EE" : "#F5FCFF" }]}
                transition="backgroundColor"
            >
                <TouchableOpacity activeOpacity={0.6} style={{ justifyContent: "center", alignItems: "center", flexDirection: "row", width: "100%" }} onPress={() => {
                    if (this.state.PerformanceHeaderTempIndex == index) {
                        this.setState({ PerformanceHeaderTempIndex: -1 })
                    } else {
                        this.setState({ PerformanceHeaderTempIndex: index, })
                    }
                }}>
                    {/*<Text style={[styles.headerText, { marginRight: 10 }]}>{this.state.TempIndex == index ? "-" : "+"}</Text>*/}
                    <View style={{ width: "90%", paddingLeft: 10, flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                        <Text style={[styles.TimeTableText, { textTransform: "uppercase" }]}>Semester {item.semester}</Text>

                    </View>
                    <View style={{ width: "10%", borderRadius: 10, alignItems: "flex-end", justifyContent: "space-between", padding: 10 }}>
                        {this.state.PerformanceHeaderTempIndex == index ?
                            <Image style={{ width: "60%", height: 20, resizeMode: "contain", borderRadius: 10, marginTop: 20 }} source={require("../assets/upArrow.png")} />
                            :
                            <Image style={{ width: "60%", height: 20, resizeMode: "contain", borderRadius: 10, marginTop: 20 }} source={require("../assets/downArrow.png")} />
                        }
                    </View>
                </TouchableOpacity>
                {this.state.PerformanceHeaderTempIndex == index ?
                    this.renderPerformanceContent(item.subjectDetails)
                    : null}
            </Animatable.View>

        })
            : !this.state.Loader ? <Animatable.View key={Math.random()}
                duration={400}
                style={[styles.header, { width: "100%", alignSelf: "center" }]}
                transition="backgroundColor"
            >
                <Text style={{ fontFamily: "Poppins-Regular", paddingLeft: 10 }}>No data avaiblabe</Text>
            </Animatable.View>
                : null
    }

    renderPerformanceContent = (marks) => {
        let PerformanceMarksArr = marks
        return (PerformanceMarksArr.length ? PerformanceMarksArr.map((item, index) => {
            console.log("item===", item);
            return <Animatable.View key={Math.random()}
                duration={400}
                style={[styles.header, { width: "100%", alignSelf: "center" }]}
                transition="backgroundColor"
            >
                <TouchableOpacity activeOpacity={0.6} style={{ justifyContent: "center", alignItems: "center", flexDirection: "row", width: "100%" }} onPress={() => {
                    //if (this.state.PerformanceMarksTempIndex == index) {
                    //    this.setState({ PerformanceMarksTempIndex: -1 })
                    //} else {
                    //    this.setState({ PerformanceMarksTempIndex: index, })
                    //}
                }}>
                    {/*<Text style={[styles.headerText, { marginRight: 10 }]}>{this.state.TempIndex == index ? "-" : "+"}</Text>*/}
                    <View style={{ width: "100%", paddingLeft: 10, justifyContent: "center", }}>
                        {item.subjectType ? <Text style={[styles.headerText, { textTransform: "none" }]}>Subject Type : {item.subjectType}</Text> : null}
                        {item.subjectName ? <Text style={[styles.headerText, { textTransform: "none" }]}>Subject Name : {item.subjectName}</Text> : null}
                        {item.subjectCode ? <Text style={[styles.headerText, { textTransform: "none" }]}>Subject Code : {item.subjectCode}</Text> : null}
                        {item.examYear ? <Text style={[styles.headerText, { textTransform: "none" }]}>Exam Year: {item.examYear}</Text> : null}
                        {item.examMonth ? <Text style={[styles.headerText, { textTransform: "none" }]}>Exam Month : {item.examMonth}</Text> : null}
                        {item.attempt ? <Text style={[styles.headerText, { textTransform: "none" }]}>Attempt : {item.attempt}</Text> : null}
                        {item.internalMark ? <Text style={[styles.headerText, { textTransform: "none" }]}>Internal Mark : {item.internalMark}</Text> : null}
                        {item.externalMark ? <Text style={[styles.headerText, { textTransform: "none" }]}>External Mark : {item.externalMark}</Text> : null}
                        {item.total ? <Text style={[styles.headerText, { textTransform: "none" }]}>Total : {item.total}</Text> : null}
                        {item.totalMark ? <Text style={[styles.headerText, { textTransform: "none" }]}>Total Mark : {item.totalMark}</Text> : null}
                        {item.grade ? <Text style={[styles.headerText, { textTransform: "none" }]}>Grade : {item.grade}</Text> : null}
                        {item.result ? <Text style={[styles.headerText, { textTransform: "none" }]}>Result : <Text style={{ fontFamily: "Poppins-Regular", color: item.result == "FAIL" ? "red" : "green" }}>{item.result}</Text></Text> : null}

                    </View>

                </TouchableOpacity>
            </Animatable.View>

        })
            : !this.state.Loader ? <Animatable.View key={Math.random()}
                duration={400}
                style={[styles.header, { width: "100%", alignSelf: "center" }]}
                transition="backgroundColor"
            >
                <Text style={{ fontFamily: "Poppins-Regular", paddingLeft: 10 }}>No data avaiblabe</Text>
            </Animatable.View>
                : null
        );
    };

    renderInternalPerformanceSubHeader = (item) => {
        let PerformanceArr = this.state.InternalPerformance;
        //let PerformanceArr = item;
        return PerformanceArr.length ? PerformanceArr.map((item, index) => {
            return item.subjectInternalMarksDetails.length ?
                item.subjectInternalMarksDetails.map((item, index) => {
                    return <Animatable.View key={Math.random()}
                        duration={400}
                        style={[styles.header, { width: "97%", alignSelf: "center", backgroundColor: this.state.InternalPerformanceSubHeaderTempIndex == index ? "#DCE8EE" : "#F5FCFF" }]}
                        transition="backgroundColor"
                    >
                        <TouchableOpacity activeOpacity={0.6} style={{ justifyContent: "center", alignItems: "center", flexDirection: "row", width: "100%" }} onPress={() => {
                            if (this.state.InternalPerformanceSubHeaderTempIndex == index) {
                                this.setState({ InternalPerformanceSubHeaderTempIndex: -1 })
                            } else {
                                this.setState({ InternalPerformanceSubHeaderTempIndex: index, })
                            }
                        }}>
                            {/*<Text style={[styles.headerText, { marginRight: 10 }]}>{this.state.TempIndex == index ? "-" : "+"}</Text>*/}
                            <View style={{ width: "90%", paddingLeft: 10, flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                                <Text style={[styles.TimeTableText, { textTransform: "uppercase" }]}>Semester {item.subjectName}</Text>

                            </View>
                            <View style={{ width: "10%", borderRadius: 10, alignItems: "flex-end", justifyContent: "space-between", padding: 10 }}>
                                {this.state.InternalPerformanceSubHeaderTempIndex == index ?
                                    <Image style={{ width: "60%", height: 20, resizeMode: "contain", borderRadius: 10, marginTop: 20 }} source={require("../assets/upArrow.png")} />
                                    :
                                    <Image style={{ width: "60%", height: 20, resizeMode: "contain", borderRadius: 10, marginTop: 20 }} source={require("../assets/downArrow.png")} />
                                }
                            </View>
                        </TouchableOpacity>
                        {this.state.InternalPerformanceSubHeaderTempIndex == index ?
                            this.renderInternalPerformanceContent(item.internalExamMarks)
                            : null}
                    </Animatable.View>
                })
                : !this.state.Loader ? <Animatable.View key={Math.random()}
                    duration={400}
                    style={[styles.header, { width: "100%", alignSelf: "center" }]}
                    transition="backgroundColor"
                >
                    <Text style={{ fontFamily: "Poppins-Regular", paddingLeft: 10 }}>No data avaiblabe</Text>
                </Animatable.View>
                    : null
        })
            : !this.state.Loader ? <Animatable.View key={Math.random()}
                duration={400}
                style={[styles.header, { width: "100%", alignSelf: "center" }]}
                transition="backgroundColor"
            >
                <Text style={{ fontFamily: "Poppins-Regular", paddingLeft: 10 }}>No data avaiblabe</Text>
            </Animatable.View>
                : null
    }
    renderUniversityMarksHeader = () => {
        let TempSem = [{ sem: "1" }, { sem: "2" }, { sem: "3" }]
        return TempSem.map((item, index) => {
            console.log(item.sem);
            return <Animatable.View key={Math.random()}
                duration={400}
                style={[styles.header, { width: "97%", alignSelf: "center", backgroundColor: this.state.UniversityMarksTempIndex == index ? "#DCE8EE" : "#F5FCFF" }]}
                transition="backgroundColor"
            >
                <TouchableOpacity activeOpacity={0.6} style={{ justifyContent: "center", alignItems: "center", flexDirection: "row", width: "100%" }} onPress={() => {
                    if (this.state.UniversityMarksTempIndex == index) {
                        this.setState({ UniversityMarksTempIndex: -1 })
                    } else {
                        this.setState({ UniversityMarksTempIndex: index, })
                    }
                }}>
                    {/*<Text style={[styles.headerText, { marginRight: 10 }]}>{this.state.TempIndex == index ? "-" : "+"}</Text>*/}
                    <View style={{ width: "90%", paddingLeft: 10, flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                        <Text style={[styles.TimeTableText, { textTransform: "uppercase" }]}>Semester {item.sem}</Text>

                    </View>
                    <View style={{ width: "10%", borderRadius: 10, alignItems: "flex-end", justifyContent: "space-between", padding: 10 }}>
                        {this.state.UniversityMarksTempIndex == index ?
                            <Image style={{ width: "60%", height: 20, resizeMode: "contain", borderRadius: 10, marginTop: 20 }} source={require("../assets/upArrow.png")} />
                            :
                            <Image style={{ width: "60%", height: 20, resizeMode: "contain", borderRadius: 10, marginTop: 20 }} source={require("../assets/downArrow.png")} />
                        }
                    </View>
                </TouchableOpacity>
                {this.state.UniversityMarksTempIndex == index ?
                    this.renderuniversityMarksContent(item.sem)
                    : null}
            </Animatable.View>
        })
    }

    renderuniversityMarksContent = (sem) => {
        let UniversityMarksArr = this.state.UniversityMarks;
        var filterred = UniversityMarksArr.filter(function (item) {
            //console.log(item.semester == sem);
            return item.semester == sem;
        }).map(function ({ rollNo, regNo, studentName, semester, subjectType, subjectName, subjectCode, examYear, examMonth, internalMark, externalMark, totalMark, result, grade, examCode, subjectNo }) {
            return { rollNo, regNo, studentName, semester, subjectType, subjectName, subjectCode, examYear, examMonth, internalMark, externalMark, totalMark, result, grade, examCode, subjectNo };
        });
        console.log("filterreduniversityMarks==========", filterred);
        return (filterred.length ? filterred.map((item, index) => {
            console.log("item===", item);
            return <Animatable.View key={Math.random()}
                duration={400}
                style={[styles.header, { width: "100%", alignSelf: "center" }]}
                transition="backgroundColor"
            >
                <TouchableOpacity activeOpacity={0.6} style={{ justifyContent: "center", alignItems: "center", flexDirection: "row", width: "100%" }} onPress={() => {
                    if (this.state.InternalMarksContentTempIndex == index) {
                        this.setState({ InternalMarksContentTempIndex: -1 })
                    } else {
                        this.setState({ InternalMarksContentTempIndex: index, })
                    }
                }}>
                    {/*<Text style={[styles.headerText, { marginRight: 10 }]}>{this.state.TempIndex == index ? "-" : "+"}</Text>*/}
                    <View style={{ width: "100%", paddingLeft: 10, justifyContent: "center", }}>
                        {item.subjectCode ? <Text style={[styles.headerText, { textTransform: "none" }]}>Subject Code : {item.subjectCode}</Text> : null}
                        {item.subjectName ? <Text style={[styles.headerText, { textTransform: "none" }]}>Subject Name : {item.subjectName}</Text> : null}
                        {item.subjectType ? <Text style={[styles.headerText, { textTransform: "none" }]}>Subject Type : {item.subjectType}</Text> : null}
                        {item.internalMark ? <Text style={[styles.headerText, { textTransform: "none" }]}>Internal Mark : {item.internalMark}</Text> : null}
                        {item.externalMark ? <Text style={[styles.headerText, { textTransform: "none" }]}>External Mark : {item.externalMark}</Text> : null}
                        {item.grade ? <Text style={[styles.headerText, { textTransform: "none" }]}>Grade : {item.grade}</Text> : null}
                        {item.result ? <Text style={[styles.headerText, { textTransform: "none" }]}>Result : {item.result}</Text> : null}
                        <Text style={[styles.headerText, { textTransform: "none" }]}>GPA/CGPA : unavailable</Text>
                    </View>

                </TouchableOpacity>
            </Animatable.View>

        })
            : !this.state.Loader ? <Animatable.View key={Math.random()}
                duration={400}
                style={[styles.header, { width: "100%", alignSelf: "center" }]}
                transition="backgroundColor"
            >
                <Text style={{ fontFamily: "Poppins-Regular", paddingLeft: 10 }}>No data avaiblabe</Text>
            </Animatable.View>
                : null
        );
    };
    renderInternalPerformanceContent = (marks) => {
        let PerformanceMarksArr = marks
        return (PerformanceMarksArr.length ? PerformanceMarksArr.map((item, index) => {
            console.log("item===", item);
            return <Animatable.View key={Math.random()}
                duration={400}
                style={[styles.header, { width: "100%", alignSelf: "center" }]}
                transition="backgroundColor"
            >
                <TouchableOpacity activeOpacity={0.6} style={{ justifyContent: "center", alignItems: "center", flexDirection: "row", width: "100%" }} onPress={() => {
                    if (this.state.InternalPerformanceMarksTempIndex == index) {
                        this.setState({ InternalPerformanceMarksTempIndex: -1 })
                    } else {
                        this.setState({ InternalPerformanceMarksTempIndex: index, })
                    }
                }}>
                    {/*<Text style={[styles.headerText, { marginRight: 10 }]}>{this.state.TempIndex == index ? "-" : "+"}</Text>*/}
                    <View style={{ width: "100%", paddingLeft: 10, justifyContent: "center", }}>
                        {item.internalTestName ? <Text style={[styles.headerText, { textTransform: "none" }]}>Internal Test Name : {item.internalTestName}</Text> : null}
                        {item.examDate ? <Text style={[styles.headerText, { textTransform: "none" }]}>Exam Date : {item.examDate}</Text> : null}
                        {item.maximumMark ? <Text style={[styles.headerText, { textTransform: "none" }]}>Maximum Mark : {item.maximumMark}</Text> : null}
                        {item.minimumMark ? <Text style={[styles.headerText, { textTransform: "none" }]}>Minimum Mark : {item.minimumMark}</Text> : null}
                        {item.studentInternalMark ? <Text style={[styles.headerText, { textTransform: "none" }]}>Student Internal Mark : {item.studentInternalMark}</Text> : null}
                        {item.result ? <Text style={[styles.headerText, { textTransform: "none", fontWeight: "600" }]}>Result : <Text style={{ fontFamily: "Poppins-Regular", textTransform: "uppercase", color: item.result == "Fail" ? "red" : "green" }}>{item.result}</Text></Text> : null}
                    </View>

                </TouchableOpacity>
            </Animatable.View>

        })
            : !this.state.Loader ? <Animatable.View key={Math.random()}
                duration={400}
                style={[styles.header, { width: "100%", alignSelf: "center" }]}
                transition="backgroundColor"
            >
                <Text style={{ fontFamily: "Poppins-Regular", paddingLeft: 10 }}>No data avaiblabe</Text>
            </Animatable.View>
                : null
        );
    };

    renderInternalMarksHeader = () => {

        var InternalMarksArr = this.state.InternalMarks;

        return (InternalMarksArr.length ? InternalMarksArr.map((item, index) => {
            console.log("item===", item);
            return <Animatable.View key={Math.random()}
                duration={400}
                style={[styles.header, { width: "97%", alignSelf: "center", backgroundColor: this.state.InternalMarksTempIndex == index ? "#DCE8EE" : "#F5FCFF" }]}
                transition="backgroundColor"
            >
                <TouchableOpacity activeOpacity={0.6} style={{ justifyContent: "center", alignItems: "center", flexDirection: "row", width: "100%" }} onPress={() => {
                    if (this.state.InternalMarksTempIndex == index) {
                        this.setState({ InternalMarksTempIndex: -1 })
                    } else {
                        this.setState({ InternalMarksTempIndex: index, })
                    }
                }}>
                    {/*<Text style={[styles.headerText, { marginRight: 10 }]}>{this.state.TempIndex == index ? "-" : "+"}</Text>*/}
                    <View style={{ width: "90%", paddingLeft: 10, flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                        <Text style={[styles.TimeTableText, { textTransform: "uppercase" }]}>{item.criteria}</Text>

                    </View>
                    <View style={{ width: "10%", borderRadius: 10, alignItems: "flex-end", justifyContent: "space-between", padding: 10 }}>
                        {this.state.InternalMarksTempIndex == index ?
                            <Image style={{ width: "60%", height: 20, resizeMode: "contain", borderRadius: 10, marginTop: 20 }} source={require("../assets/upArrow.png")} />
                            :
                            <Image style={{ width: "60%", height: 20, resizeMode: "contain", borderRadius: 10, marginTop: 20 }} source={require("../assets/downArrow.png")} />
                        }
                    </View>
                </TouchableOpacity>
                {this.state.InternalMarksTempIndex == index ?
                    this.renderInternalMarksContent(item.criteriaDetails)
                    : null}
            </Animatable.View>

        }) : !this.state.Loader ? <Animatable.View key={Math.random()}
            duration={400}
            style={[styles.header, { width: "100%", alignSelf: "center" }]}
            transition="backgroundColor"
        >
            <Text style={[styles.headerText, { textTransform: "none" }]}>Oops! No data avaiblabe</Text>
        </Animatable.View>
                : null
        );
    };
    renderInternalMarksContent = (subjectName) => {
        let InternalMarksArr = subjectName;
        //var filterred = InternalMarksArr.filter(function (item) {
        //    console.log(item.criteria == subjectName);
        //    return item.criteria == subjectName;
        //}).map(function ({ rollNo, regNo, studentName, currentSemester, criteria, subjectCode, subjectName, examDate, maxMark, minMark, aaa, mark, result, criteriaNo, examCode, subjectNo }) {
        //    return { rollNo, regNo, studentName, currentSemester, criteria, subjectCode, subjectName, examDate, maxMark, minMark, aaa, mark, result, criteriaNo, examCode, subjectNo };
        //});
        //console.log("filterredInternalMarks==========", filterred);
        return (
            InternalMarksArr.length ? InternalMarksArr.map((item, index) => {
                return <Animatable.View key={Math.random()}
                    duration={400}
                    style={[styles.header, { width: "100%", alignSelf: "center" }]}
                    transition="backgroundColor"
                >
                    <TouchableOpacity activeOpacity={0.6} style={{ justifyContent: "center", alignItems: "center", flexDirection: "row", width: "100%" }} onPress={() => {
                        if (this.state.InternalMarksContent2TempIndex == index) {
                            this.setState({ InternalMarksContent2TempIndex: -1 })
                        } else {
                            this.setState({ InternalMarksContent2TempIndex: index, })
                        }
                    }}>
                        {/*<Text style={[styles.headerText, { marginRight: 10 }]}>{this.state.TempIndex == index ? "-" : "+"}</Text>*/}
                        <View style={{ width: "100%", paddingLeft: 10, justifyContent: "center", }}>
                            <Text style={[styles.headerText, { textTransform: "none" }]}>Serial No : {index + 1}</Text>
                            {item.examCode ? <Text style={[styles.headerText, { textTransform: "none" }]}>Exam Date : {item.examDate}</Text> : null}
                            {item.examDate ? <Text style={[styles.headerText, { textTransform: "none" }]}>Exam Code : {item.examCode}</Text> : null}
                            {item.subjectName ? <Text style={[styles.headerText, { textTransform: "none" }]}>Subject Name : {item.subjectName}</Text> : null}
                            {item.subjectCode ? <Text style={[styles.headerText, { textTransform: "none" }]}>Subject Code : {item.subjectCode}</Text> : null}
                            {item.subjectNo ? <Text style={[styles.headerText, { textTransform: "none" }]}>Subject Number : {item.subjectNo}</Text> : null}
                            {item.maxMark ? <Text style={[styles.headerText, { textTransform: "none" }]}>Max Mark : {item.maxMark}</Text> : null}
                            {item.minMark ? <Text style={[styles.headerText, { textTransform: "none" }]}>Min Mark : {item.minMark}</Text> : null}
                            {item.mark ? <Text style={[styles.headerText, { textTransform: "none" }]}>Mark : {item.mark}</Text> : null}
                            {item.result ? <Text style={[styles.headerText, { textTransform: "none" }]}>Result : {item.result}</Text> : null}
                        </View>

                    </TouchableOpacity>
                </Animatable.View>

            })
                : !this.state.Loader ? <Animatable.View key={Math.random()}
                    duration={400}
                    style={[styles.header, { width: "100%", alignSelf: "center" }]}
                    transition="backgroundColor"
                >
                    <Text style={[styles.headerText, { textTransform: "none" }]}>Oops! No data avaiblabe</Text>
                </Animatable.View>
                    : null
        );
    };
    renderAttendanceHeader = () => {
        const startDate = moment(this.state.AttendanceStartDate);
        const timeEnd = moment(this.state.AttendanceEndDate);
        const diff = timeEnd.diff(startDate);
        const diffDuration = moment.duration(diff);

        console.log("Days:", diffDuration.days());
        const DaysDifference = diffDuration.days()
        //this.setState({DaysDifference:diffDuration.days()},function(){
        //    alert(this.state.DaysDifference)
        //})
        return <Animatable.View key={Math.random()}
            duration={400}
            style={[styles.header, { width: "90%", alignSelf: "center", backgroundColor: this.state.AttendanceStartDate != "" && this.state.AttendanceEndDate != "" ? "#DCE8EE" : "#F5FCFF" }]}
            transition="backgroundColor"
        >
            <View style={{ justifyContent: "center", width: "100%" }}>

                <Text style={[styles.TimeTableText, { textTransform: "uppercase" }]}>{DaysDifference < 8 ? "Weekly Attendance" : "Selected Period Attendance"}</Text>
                <View style={{ width: "100%", flexDirection: "row", justifyContent: "space-evenly", alignItems: "center", marginVertical: 10 }}>
                    <View style={{ width: "50%", justifyContent: "center" }}>
                        <DatePicker
                            style={{ width: 160 }}
                            date={this.state.AttendanceStartDate}
                            mode="date"
                            placeholder="from date"
                            format="YYYY-MM-DD"
                            minDate="2020-01-01"
                            //maxDate={new Date()}
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            showIcon={false}
                            onDateChange={(date) => {
                                this.setState({ AttendanceStartDate: date, ApiRequestRequired: true, Loader: true }, function () {
                                    this.state.AttendanceStartDate != "" && this.state.AttendanceEndDate != "" && this.state.ApiRequestRequired ?
                                        this.onPressGetAttendance()
                                        : null
                                })
                            }}
                        />
                    </View>
                    <View style={{ width: "50%", justifyContent: "center", alignItems: "flex-end" }}>
                        <DatePicker
                            style={{ width: 160 }}
                            date={this.state.AttendanceEndDate}
                            mode="date"
                            placeholder="to date"
                            format="YYYY-MM-DD"
                            minDate="2020-01-01"
                            //maxDate={new Date()}
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            showIcon={false}
                            onDateChange={(date) => {
                                this.setState({ AttendanceEndDate: date, ApiRequestRequired: true, Loader: true }, function () {
                                    this.state.AttendanceStartDate != "" && this.state.AttendanceEndDate != "" && this.state.ApiRequestRequired ?
                                        this.onPressGetAttendance()
                                        : null
                                })
                            }}
                        />
                    </View>
                </View>
            </View>
            {!this.state.ApiRequestRequired ?
                this.renderAttendanceContent()
                : null}
        </Animatable.View>

    };

    renderAttendanceContent = () => {
        let AttendanceArr = this.state.Attendance;
        //var filterred = AttendanceArr.filter(function (item) {
        //    console.log(item.attdate == date);
        //    return item.attdate == date;
        //}).map(function ({ rollNo, regNo, attdate, h1, h2, h3, h4, h5, h6, h7, h8, h9, h10 }) {
        //    return { rollNo, regNo, attdate, h1, h2, h3, h4, h5, h6, h7, h8, h9, h10 };
        //});
        //console.log("filterredCamContent==========", filterred);
        //return (
        return <Animatable.View key={Math.random()}
            duration={400}
            style={[styles.header, { width: "100%", alignSelf: "center", backgroundColor: "#DCE8EE" }]}
            transition="backgroundColor"
        >
            {AttendanceArr.length ? AttendanceArr.map((item, index) => {
                return <Animatable.View key={Math.random()}
                    duration={400}
                    style={[styles.header, { width: "100%", alignSelf: "center" }]}
                    transition="backgroundColor"
                >
                    <TouchableOpacity activeOpacity={1} style={{ justifyContent: "center", alignItems: "center", flexDirection: "row", width: "100%" }} onPress={() => {
                    }}>
                        <View style={{ width: "100%", paddingLeft: 10, justifyContent: "center", }}>

                            {item.rollNo ? <Text style={[styles.headerText, { textTransform: "none" }]}>RollNo : {item.rollNo}</Text> : null}
                            {item.regNo ? <Text style={[styles.headerText, { textTransform: "none" }]}>Reg No : {item.regNo}</Text> : null}
                            {item.attdate ? <Text style={[styles.headerText, { textTransform: "none" }]}>Attendace date : {item.attdate}</Text> : null}
                            {item.h1 ? <Text style={[styles.headerText, { textTransform: "none" }]}>H1 : {item.h1}</Text> : null}
                            {item.h2 ? <Text style={[styles.headerText, { textTransform: "none" }]}>H2 : {item.h2}</Text> : null}
                            {item.h3 ? <Text style={[styles.headerText, { textTransform: "none" }]}>H3 : {item.h3}</Text> : null}
                            {item.h4 ? <Text style={[styles.headerText, { textTransform: "none" }]}>H4 : {item.h4}</Text> : null}
                            {item.h5 ? <Text style={[styles.headerText, { textTransform: "none" }]}>H5 : {item.h5}</Text> : null}
                            {item.h6 ? <Text style={[styles.headerText, { textTransform: "none" }]}>H6 : {item.h6}</Text> : null}
                            {item.h7 ? <Text style={[styles.headerText, { textTransform: "none" }]}>H7 : {item.h7}</Text> : null}
                            {item.h8 ? <Text style={[styles.headerText, { textTransform: "none" }]}>H8 : {item.h8}</Text> : null}
                            {item.h9 ? <Text style={[styles.headerText, { textTransform: "none" }]}>H9 : {item.h9}</Text> : null}
                            {item.h10 ? <Text style={[styles.headerText, { textTransform: "none" }]}>H10 : {item.h10}</Text> : null}
                        </View>

                    </TouchableOpacity>
                </Animatable.View>

            })
                :
                !this.state.Loader ? <Animatable.View key={Math.random()}
                    duration={400}
                    style={[styles.header, { width: "100%", alignSelf: "center" }]}
                    transition="backgroundColor"
                >
                    <Text style={[styles.headerText, { textTransform: "none" }]}>Oops! No data avaiblabe for selected time period</Text>
                </Animatable.View>
                    : null
            }
            {this.state.AttendanceData.totalHours ? <Animatable.View key={Math.random()}
                duration={400}
                style={[styles.header, { width: "100%", alignSelf: "center", backgroundColor: "#DCE8EE" }]}
                transition="backgroundColor"
            >
                <TouchableOpacity activeOpacity={1} style={{ justifyContent: "center", alignItems: "center", flexDirection: "row", width: "100%" }} onPress={() => {
                }}>
                    <View style={{ width: "100%", marginLeft: 15, paddingLeft: 15, justifyContent: "center", }}>

                        {this.state.AttendanceData.totalHours ? <Text style={[styles.headerText, { textTransform: "none", fontWeight: "600", fontSize: ResponsiveSize(16), marginLeft: -20 }]}>Total hours : {this.state.AttendanceData.totalHours}</Text> : null}
                        {this.state.AttendanceData.totalHoursPresent ? <Text style={[styles.headerText, { textTransform: "none", fontWeight: "600", fontSize: ResponsiveSize(16), marginLeft: -20 }]}>Total present hours : {this.state.AttendanceData.totalHoursPresent}</Text> : null}
                        {this.state.AttendanceData.totalHourseAbsent ? <Text style={[styles.headerText, { textTransform: "none", fontWeight: "600", fontSize: ResponsiveSize(16), marginLeft: -20 }]}>Total absent hours : {this.state.AttendanceData.totalHourseAbsent}</Text> : null}
                    </View>

                </TouchableOpacity>
            </Animatable.View>
                : null}
        </Animatable.View>


    };

    renderHeader3 = () => {
        return (<View style={styles.SubjectMainContainer}>
            <Image style={styles.SubjectNameImageView} source={require('../assets/purpleBg.png')}>
            </Image>
            <View style={styles.SubjectNameTextView}>
                <Text numberOfLines={1} style={styles.SubjectNameTextStyle}>{this.state.TempTimeTable.day}</Text>
            </View>
            {this.state.CamTimeTable.length ?
                this.state.CamTimeTable.map((item, index) => {
                    //console.log("Camtimetable ==== ", item);
                    return <View style={{ width: "90%", alignItems: "center", margin: this.state.CamTimeTableTempIndex == index ? 10 : 0, borderRadius: 10, backgroundColor: this.state.CamTimeTableTempIndex == index ? PurpleColor : "transparent", paddingBottom: 15 }}>
                        <Animatable.View key={Math.random()}
                            duration={400}
                            style={{ width: "100%", alignSelf: "center", backgroundColor: index < 19 ? this.state.PurpleColors[index] : (this.state.PurpleColors[Math.floor(Math.random() * this.state.PurpleColors.length)]), elevation: 3, shadowColor: "silver", shadowOpacity: .6, shadowOffset: { width: 1, height: 1 }, justifyContent: "center", alignItems: "center", borderRadius: 10, padding: 10, minHeight: 90, }}
                            transition="backgroundColor"
                        >
                            <TouchableOpacity activeOpacity={0.6} style={{ justifyContent: "center", alignItems: "center", flexDirection: "row", width: "100%" }} onPress={() => {
                                if (this.state.CamTimeTableTempIndex == index) {
                                    this.setState({ CamTimeTableTempIndex: -1 })
                                } else {
                                    this.setState({ CamTimeTableTempIndex: index, })
                                }
                            }}>
                                {/*<Text style={[styles.headerText, { marginRight: 10 }]}>{this.state.TempIndex == index ? "-" : "+"}</Text>*/}
                                <View style={{ width: "100%", paddingLeft: 10, flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                                    <View style={{ width: "50%", justifyContent: "center" }}>
                                        {/*<Text style={[styles.TimeTableText, { fontWeight: "600" }]}>Exam name : </Text>*/}
                                        <Text style={[styles.TimeTableText, { color: PurpleColor }]}>{item.internalTestName}</Text>
                                    </View>
                                    <View style={{ justifyContent: "center", alignItems: "center", paddingRight: 20, width: "50%" }}>
                                        <View style={{ justifyContent: "center", alignItems: "center", }}>
                                            <Text style={[styles.headerText, { textTransform: "none", color: PurpleColor }]}>{item.examStartDate}</Text>
                                            <Text style={[styles.headerText, { textTransform: "none", padding: 5, color: PurpleColor }]}>to</Text>
                                            <Text style={[styles.headerText, { textTransform: "none", color: PurpleColor }]}>{item.examEndDate}</Text>
                                        </View>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </Animatable.View>
                        {this.state.CamTimeTableTempIndex == index ?
                            this.renderCamTestDetailsContent(item.testDetails, item.internalTestName)
                            : null}
                    </View>

                }).reverse()
                : !this.state.Loader ? <Animatable.View key={Math.random()}
                    duration={400}
                    style={[styles.header, { width: "95%", alignSelf: "center" }]}
                    transition="backgroundColor"
                >
                    <Text style={[styles.headerText, { textTransform: "none" }]}>Oops! No data avaiblabe</Text>
                </Animatable.View>
                    : null}
        </View>
        );
    };
    //renderHeader4 = () => {

    //    return (
    //        this.state.UniversityTimeTable.map((item, index) => {
    //            return <Animatable.View key={Math.random()}
    //                duration={400}
    //                style={[styles.header, { width: "90%", alignSelf: "center" }]}
    //                transition="backgroundColor"
    //            >
    //                <TouchableOpacity activeOpacity={0.6} style={{ justifyContent: "center", alignItems: "center", flexDirection: "row", width: "100%" }} onPress={() => {
    //                    if (this.state.UniversityTimeTableTempIndex == index) {
    //                        this.setState({ UniversityTimeTableTempIndex: -1 })
    //                    } else {
    //                        this.setState({ UniversityTimeTableTempIndex: index, })
    //                    }
    //                }}>
    //                    {/*<Text style={[styles.headerText, { marginRight: 10 }]}>{this.state.TempIndex == index ? "-" : "+"}</Text>*/}
    //                    <View style={{ width: "90%", paddingLeft: 10, flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
    //                        <Text style={[styles.TimeTableText, { textTransform: "uppercase" }]}>Semester : {item.semester}</Text>

    //                    </View>
    //                    <View style={{ width: "10%", borderRadius: 10, alignItems: "flex-end", justifyContent: "space-between", padding: 10 }}>
    //                        {this.state.UniversityTimeTableTempIndex == index ?
    //                            <Image style={{ width: "60%", height: 20, resizeMode: "contain", borderRadius: 10, marginTop: 20 }} source={require("../assets/upArrow.png")} />
    //                            :
    //                            <Image style={{ width: "60%", height: 20, resizeMode: "contain", borderRadius: 10, marginTop: 20 }} source={require("../assets/downArrow.png")} />
    //                        }
    //                    </View>
    //                </TouchableOpacity>
    //                {this.state.UniversityTimeTableTempIndex == index ?
    //                    this.UniversityrenderContent(item.semester)
    //                    : null}
    //            </Animatable.View>

    //        })
    //    );
    //};

    UniversityrenderContent(filterred) {
        return (<View style={styles.SubjectMainContainer}>
            <Image style={styles.SubjectNameImageView} source={require('../assets/greenBg.png')}>
            </Image>
            <View style={styles.SubjectNameTextView}>
                <Text numberOfLines={1} style={styles.SubjectNameTextStyle}>{this.state.TempTimeTable.day}</Text>
            </View>
            {filterred.length ?
                filterred.map((item, index) => {
                    return <Animatable.View key={Math.random()}
                        duration={400}
                        style={{ width: "95%", elevation: 3, padding: 10, minHeight: 90, shadowColor: "silver", shadowOpacity: .6, shadowOffset: { width: 1, height: 1 }, borderRadius: 5, alignSelf: "center", alignItems: "center", marginTop: 15, flexDirection: "row", backgroundColor: index < 19 ? this.state.Colors[index] : (this.state.Colors[Math.floor(Math.random() * this.state.Colors.length)]) }}
                        transition="backgroundColor"
                    >
                        <View style={{ paddingHorizontal: 10, width: "50%" }}>
                            {item.subjectName ? <Text style={[styles.headerText, { textTransform: "none", fontSize: ResponsiveSize(14), color: "black", fontWeight: "600" }]}>{item.subjectName}</Text> : null}
                            {/*{item.subjectCode ? <Text style={[styles.headerText, { textTransform: "none" }]}>{item.subjectCode}</Text> : null}*/}
                            <View style={{ width: "100%", alignItems: "center", flexDirection: "row", marginVertical: 5 }}>
                                {item.startTime ? <Text style={[styles.headerText, { textTransform: "none", color: GreenColor }]}>{moment(item.startTime).format("hh:mm")}</Text> : null}
                                {item.endTimes ? <Text style={[styles.headerText, { textTransform: "none", color: GreenColor }]}> to </Text> : null}
                                {item.endTimes ? <Text style={[styles.headerText, { textTransform: "none", color: GreenColor }]}>{moment(item.endTimes).format("hh:mm")}</Text> : null}
                            </View>
                            {item.examSession ? <Text style={[styles.headerText, { textTransform: "none", color: "gray" }]}>Session {item.examSession}</Text> : null}

                        </View>
                        <View style={{ alignItems: "flex-end", width: "50%", paddingRight: 10 }}>
                            {item.examDate ? <Text style={[styles.headerText, { textTransform: "none", fontWeight: "600" }]}>{moment(item.examDate).format("DD/MM/YYYY")}</Text> : null}
                            {item.semester ? <Text style={[styles.headerText, { textTransform: "none", color: GreenColor, marginVertical: 5 }]}>Semester {item.semester}</Text> : null}
                            {item.subjectType ? <Text style={[styles.headerText, { textTransform: "none" }]}>{item.subjectType}</Text> : null}
                        </View>
                    </Animatable.View>



                }) : !this.state.Loader ? <Animatable.View key={Math.random()}
                    duration={400}
                    style={[styles.header, { width: "95%", alignSelf: "center" }]}
                    transition="backgroundColor"
                >
                    <Text style={[styles.headerText, { textTransform: "none" }]}>Oops! No data avaiblabe</Text>
                </Animatable.View> : null}
        </View>
        )
    }
    renderContent3(topicNumber) {
        let SubjectArr = this.state.SubjectsDetail;
        var filterredSubUnitName = SubjectArr.filter(function (item) {
            console.log(item.parentCode == topicNumber);
            return item.parentCode == topicNumber;
        }).map(function ({ subjectCode, subjectName, topicNumber, unitName }) {
            return { subjectCode, subjectName, topicNumber, unitName };
        });
        console.log("filterredSubUnitName==========", filterredSubUnitName);
        return (
            <Animatable.View
                duration={400}
                style={{ width: "95%", alignSelf: "center" }}
                transition="backgroundColor"
            >
                {filterredSubUnitName.length ? filterredSubUnitName.map((item, index) => {
                    return <Animatable.View key={Math.random()}
                        duration={400}
                        style={{ width: "100%", }}
                        transition="backgroundColor"
                    >
                        <View>
                            <Text style={{ fontFamily: "Poppins-Regular", textAlign: "left", padding: 10, }}>{item.unitName}</Text>
                        </View>
                    </Animatable.View>


                }) : !this.state.Loader ? <Animatable.View key={Math.random()}
                    duration={400}
                    style={[styles.header, { width: "95%", alignSelf: "center" }]}
                    transition="backgroundColor"
                >
                    <Text style={[styles.headerText, { textTransform: "none" }]}>Oops! No data avaiblabe</Text>
                </Animatable.View> : null}
            </Animatable.View>
        )
    }
    renderContent4(topicNumber) {
        let SubjectArr = this.state.SubjectsDetail;
        var filterredSub2UnitName = SubjectArr.filter(function (item) {
            console.log(item.parentCode == topicNumber);
            return item.parentCode == topicNumber;
        }).map(function ({ subjectCode, subjectName, topicNumber, unitName }) {
            return { subjectCode, subjectName, topicNumber, unitName };
        });
        console.log("filterredSub2UnitName=====================", filterredSub2UnitName);
        return (
            <Animatable.View
                duration={400}
                style={[styles.content2, styles.active]}
                transition="backgroundColor"
            >
                {filterredSub2UnitName.length ? filterredSub2UnitName.map((item, index) => {
                    return <Animatable.View key={Math.random()}
                        duration={400}
                        style={[styles.header, styles.active]}
                        transition="backgroundColor"
                    ><TouchableOpacity style={{ justifyContent: "flex-start", alignItems: "center", flexDirection: "row" }} onPress={() => {
                        if (this.state.filterredSub2UnitNameTempIndex == index) {
                            this.setState({ filterredSub2UnitNameTempIndex: -1 })
                        } else {
                            this.setState({ filterredSub2UnitNameTempIndex: index })
                        }
                    }}>

                            <Text style={[styles.headerText, { marginRight: 10 }]}>{this.state.filterredSub2UnitNameTempIndex == index ? "-" : "+"}</Text>
                            <Text style={[styles.headerText, { textAlign: "left", backgroundColor: '#DCE8EE', padding: 10, width: "90%" }]}>{item.unitName}</Text>
                        </TouchableOpacity>
                    </Animatable.View>

                }) : !this.state.Loader ? <Animatable.View key={Math.random()}
                    duration={400}
                    style={[styles.header, { width: "95%", alignSelf: "center" }]}
                    transition="backgroundColor"
                >
                    <Text style={[styles.headerText, { textTransform: "none" }]}>Oops! No data avaiblabe</Text>
                </Animatable.View> : null}
            </Animatable.View>
        )
    }

    renderHeaderOfSubject() {
        return <View style={styles.SubjectMainContainer}>
            <Image style={styles.SubjectNameImageView} source={require('../assets/greenBg.png')}>
            </Image>
            <View style={styles.SubjectNameTextView}>
                <Text numberOfLines={1} style={styles.SubjectNameTextStyle}>{this.state.Subjects.subjectName}</Text>
            </View>
            <Image style={[styles.SubjectNameImageView, { height: 160 }]} source={require('../assets/greenBgLarge.png')}>
            </Image>
            <View style={[styles.SubjectNameImageView, { width: "93%", position: "absolute", height: 160, marginTop: 90 }]}>
                <View style={{ width: "100%", height: "100%", flexDirection: "row", justifyContent: "space-between", padding: 20, alignItems: "center" }}>
                    <View style={{ width: "70%", height: "100%", justifyContent: "center" }}>
                        <View style={{ width: "100%", height: "70%", }}>
                            {this.state.Subjects.staffName ? <Text style={styles.StaffDetailTextStyle}>{this.state.Subjects.staffName}</Text> : null}
                            {this.state.Subjects.mailId ? <Text style={styles.StaffDetailTextStyle}>{this.state.Subjects.mailId}</Text> : null}
                            {this.state.Subjects.mobile ? <Text style={styles.StaffDetailTextStyle}>{this.state.Subjects.mobile}</Text> : null}
                        </View>
                        <View style={{ width: "100%", height: "30%", }}>
                            {this.state.Subjects.subjectType ? <Text style={styles.StaffDetailTextStyle}>Subject Type : {this.state.Subjects.subjectType}</Text> : null}
                            {this.state.Subjects.subjectCode ? <Text style={styles.StaffDetailTextStyle}>{this.state.Subjects.subjectCode}</Text> : null}
                        </View>
                    </View>
                    <View style={{ width: "30%", height: "100%", justifyContent: "center", alignItems: "flex-end" }}>
                        <View style={{ width: 90, height: 90, justifyContent: "center", alignItems: "center", borderRadius: 90, backgroundColor: "#57C7D4" }}>
                            {/*<Image style={{ width: 80, height: 80, resizeMode: "stretch", borderRadius: 80, }} source={require('../assets/user.png')}>
                            </Image>*/}
                        </View>
                    </View>
                </View>
            </View>
            <View style={{ marginTop: 20, width: "100%", alignSelf: "center" }}>
                {this.renderContent()}
            </View>
        </View>
    }
    renderContent() {

        let SubjectArr = this.state.SubjectsDetail;
        let SubjectName = this.state.Subjects.subjectName;
        var filterredSub = SubjectArr.filter(function (item) {
            return SubjectName == item.subjectName;
        }).map(function ({ subjectCode, subjectName, topicNumber, unitName, parentCode }) {
            return { subjectCode, subjectName, topicNumber, unitName, parentCode };
        });
        console.log("filterredSub==", filterredSub);
        filterredSub.map((MainItem, MainIndex) => {
            return filterredUnitName = SubjectArr.filter(function (item) {
                //console.log(item.parentCode == topicNumber);
                return MainItem.parentCode == item.topicNumber;
            }).map(function ({ subjectCode, subjectName, topicNumber, unitName }) {
                return { subjectCode, subjectName, topicNumber, unitName };
            });
        })
        console.log("filterredUnitName==", filterredUnitName);
        return (
            <Animatable.View key={Math.random()}
                duration={400}
                style={[styles.SubjectNameContainer, { borderRadius: 10, backgroundColor: this.state.filterredSubTempIndex != -1 ? "#23C4D7" : "transparent", }]}
                transition="backgroundColor"
            >

                {filterredUnitName.length ? filterredUnitName.map((item, index) => {
                    return <Animatable.View key={Math.random()}
                        duration={400}
                        style={[styles.SubjectNameContainer, { backgroundColor: "transparent", width: "100%" }]}
                        transition="backgroundColor"
                    >
                        <TouchableOpacity style={{ width: "100%", height: 70, backgroundColor: index < 19 ? this.state.Colors[index] : (this.state.Colors[Math.floor(Math.random() * this.state.Colors.length)]), elevation: 3, shadowColor: "silver", shadowOpacity: .6, shadowOffset: { width: 1, height: 1 }, justifyContent: "center", alignItems: "center", borderRadius: 5, padding: 10 }} onPress={() => {
                            if (this.state.filterredSubTempIndex == index) {
                                this.setState({ filterredSubTempIndex: -1, filterredUnitNameTempIndex: -1, filterredSubUnitNameTempIndex: -1 })
                            } else {
                                this.setState({ filterredSubTempIndex: index })
                            }
                        }}>
                            <View style={{ height: "100%", width: "100%", marginVertical: 10, flexDirection: "row", alignItems: "center", padding: 5 }}>
                                <View style={{ width: 30, height: 30, borderRadius: 30, backgroundColor: "#0A9BAC", justifyContent: "center", alignItems: "center" }}>
                                    <Text style={{ fontFamily: "Poppins-Regular", color: "white" }}>{index + 1}</Text>
                                </View>
                                <Text numberOfLines={1} style={{ color: "#23C4D7", fontWeight: "600", fontSize: ResponsiveSize(13), paddingHorizontal: 5 }}>{item.unitName}</Text>
                            </View>
                        </TouchableOpacity>
                        {this.state.filterredSubTempIndex == index ?
                            filterredUnitName.map((i, x) => {
                                return <Animatable.View key={Math.random()}
                                    duration={400}
                                    style={{ backgroundColor: "white", width: "97%", alignSelf: "center", borderRadius: 5, marginTop: 10 }}
                                    transition="backgroundColor"
                                ><TouchableOpacity style={{ width: "100%", height: 65, backgroundColor: "#D3F3F7", elevation: 3, shadowColor: "silver", shadowOpacity: .6, shadowOffset: { width: 1, height: 1 }, alignItems: "center", borderRadius: 5, padding: 10, flexDirection: "row" }} onPress={() => {
                                    if (this.state.filterredUnitNameTempIndex == index) {
                                        this.setState({ filterredUnitNameTempIndex: -1 })
                                    } else {
                                        this.setState({ filterredUnitNameTempIndex: index })
                                    }
                                }}>
                                        <View style={{ height: "100%", width: "5%", justifyContent: "center" }}>
                                            <Text numberOfLines={1} style={{ color: "#23C4D7", fontWeight: "600", fontSize: ResponsiveSize(28) }}>|</Text>
                                        </View>
                                        <View style={{ height: "100%", width: "95%", justifyContent: "center" }}>
                                            <Text numberOfLines={1} style={{ color: "#23C4D7", fontWeight: "600", fontSize: ResponsiveSize(13), paddingHorizontal: 10 }}>{i.unitName}</Text>
                                        </View>
                                    </TouchableOpacity>
                                    {this.state.filterredUnitNameTempIndex == index ?
                                        this.renderContent3(i.topicNumber)
                                        : null}
                                </Animatable.View>

                            })
                            : null}
                    </Animatable.View>
                }) : !this.state.Loader ? <Animatable.View key={Math.random()}
                    duration={400}
                    style={[styles.header, { width: "95%", alignSelf: "center" }]}
                    transition="backgroundColor"
                >
                    <Text style={[styles.headerText, { textTransform: "none" }]}>Oops! No data avaiblabe</Text>
                </Animatable.View> : null}
            </Animatable.View>

        );
    }
    TimeTablePreRenderContent() {

        return (
            <View style={styles.SubjectMainContainer}>
                <Image style={styles.SubjectNameImageView} source={require('../assets/greenBg.png')}>
                </Image>
                <View style={styles.SubjectNameTextView}>
                    <Text numberOfLines={1} style={styles.SubjectNameTextStyle}>{this.state.TempTimeTable.day}</Text>
                </View>

                {this.state.TodayTimeTable ?
                    this.state.TodayTimeTable.map((MainItem, MainIndex) => {
                        let TodayTimeTable = this.state.TodayTimeTable;
                        var filterred = TodayTimeTable.filter(function (item) {
                            return item.hour == MainItem.hour;
                        }).map(function ({ date, endTime, hour, staffName, startTime, subject }) {
                            return { date, endTime, hour, staffName, startTime, subject };
                        });
                        console.log("TimeTableByHours===", filterred);

                        return filterred.map((item, index) => {
                            return <Animatable.View key={Math.random()}
                                duration={400}
                                style={{ width: "95%", padding: 10, minHeight: 90, borderRadius: 10, marginTop: 15, elevation: 3, shadowColor: "silver", shadowOpacity: .4, shadowOffset: { width: 1, height: 1 }, alignSelf: "center", flexDirection: "row", alignItems: "center", backgroundColor: index < 19 ? this.state.Colors[MainIndex] : (this.state.Colors[Math.floor(Math.random() * this.state.Colors.length)]) }}
                                transition="backgroundColor"
                            >
                                <View style={{ width: "20%", justifyContent: "center", alignItems: "center" }}>
                                    <View style={{ width: 40, height: 40, borderRadius: 40, justifyContent: "center", alignItems: "center", backgroundColor: "#23C4D7" }}>
                                        {MainItem.hour ? <Text style={{ fontFamily: "Poppins-Regular", color: "white", fontWeight: "600" }}>H{MainItem.hour}</Text> : null}
                                    </View>
                                </View>
                                <View style={{ width: "40%", justifyContent: "center" }}>
                                    {item.subject ? <Text style={{ fontFamily: "Poppins-Regular", fontWeight: "600", color: "#23C4D7", fontSize: ResponsiveSize(14), paddingRight: 10 }}>{item.subject}</Text> : null}
                                    {/*{item.staffName ? <Text style={{fontFamily: "Poppins-Regular", fontWeight: "600" }}>{item.staffName}</Text> : null}*/}

                                </View>
                                <View style={{ width: "40%", justifyContent: "center", paddingRight: 10 }}>
                                    <View style={{ justifyContent: "center", flexDirection: "row" }}>
                                        {MainItem.startTime ? <Text >{moment(MainItem.startTime).format("hh:mm a")} to </Text> : null}
                                        {MainItem.endTime ? <Text style={[styles.headerText, { textTransform: "none" }]}>{moment(MainItem.endTime).format("hh:mm a")}</Text> : null}
                                    </View>
                                    {item.staffName ? <Text style={{ fontFamily: "Poppins-Regular", color: "#23C4D7", marginVertical: 5 }}>{item.staffName}</Text> : null}

                                </View>
                            </Animatable.View>
                        })
                    })
                    : !this.state.Loader ? <Animatable.View key={Math.random()}
                        duration={400}
                        style={[styles.header, { width: "95%", alignSelf: "center" }]}
                        transition="backgroundColor"
                    >
                        <Text style={[styles.headerText, { textTransform: "none" }]}>Oops! No data avaiblabe</Text>
                    </Animatable.View> : null}
            </View>
        )
    }
    //TimeTableRenderContent(hour) {
    //    let TodayTimeTable = this.state.TodayTimeTable;
    //    var filterred = TodayTimeTable.filter(function (item) {
    //        return item.hour == hour;
    //    }).map(function ({ date, endTime, hour, staffName, startTime, subject }) {
    //        return { date, endTime, hour, staffName, startTime, subject };
    //    });
    //    console.log("TimeTableByHours===", filterred);
    //    return (
    //        <View>
    //            {filterred.length ? filterred.map((item, index) => {
    //                //console.log("item==", item);
    //                return <View style={{ margin: 5, width: "100%", alignSelf: "center", borderRadius: 5, padding: 10 }} key={Math.random()}>
    //                    {/*<Text style={[styles.TimeTableText]}>Date - {item.date}</Text>*/}
    //                    {item.subject ? <Text style={[styles.headerText, { textTransform: "none" }]}>Subject name - {item.subject}</Text> : null}
    //                    {item.staffName ? <Text style={[styles.headerText, { textTransform: "none" }]}>Staff name - {item.staffName}</Text> : null}
    //                </View>
    //            }) : !this.state.Loader ? <Animatable.View key={Math.random()}
    //                duration={400}
    //                style={[styles.header, { width: "95%", alignSelf: "center" }]}
    //                transition="backgroundColor"
    //            >
    //                <Text style={[styles.headerText, { textTransform: "none" }]}>Oops! No data avaiblabe</Text>
    //            </Animatable.View> : null}
    //        </View>
    //    );
    //}

    renderContent2(section) {
        return (
            <Animatable.View
                duration={400}
                style={[styles.content, styles.inactive]}
                transition="backgroundColor"
            >
                {/*<Animatable.Text animation={isActive ? 'bounceIn' : undefined}>*/}
                {section.dayDetails.length ? section.dayDetails.map((item, index) => {
                    console.log("item==", item);
                    return <Animatable.View key={Math.random()}
                        duration={400}
                        style={{ width: "95%", padding: 10, minHeight: 90, borderRadius: 10, marginTop: 15, elevation: 3, shadowColor: "silver", shadowOpacity: .4, shadowOffset: { width: 1, height: 1 }, alignSelf: "center", flexDirection: "row", alignItems: "center", backgroundColor: index < 19 ? this.state.BlueColors[index] : (this.state.BlueColors[Math.floor(Math.random() * this.state.BlueColors.length)]) }}
                        transition="backgroundColor"
                    >
                        <View style={{ width: "20%", justifyContent: "center", alignItems: "center" }}>
                            <View style={{ width: 40, height: 40, borderRadius: 40, justifyContent: "center", alignItems: "center", backgroundColor: BlueColor }}>
                                {item.hour ? <Text style={{ fontFamily: "Poppins-Regular", color: "white", fontWeight: "600" }}>H{item.hour}</Text> : null}
                            </View>
                        </View>
                        <View style={{ width: "40%", justifyContent: "center" }}>
                            {item.subjectName ? <Text style={{ fontFamily: "Poppins-Regular", fontWeight: "600", color: BlueColor, fontSize: ResponsiveSize(14), paddingRight: 10 }}>{item.subjectName}</Text> : null}
                            {item.subjectCode ? <Text style={{ fontFamily: "Poppins-Regular", marginVertical: 5 }} >{item.subjectCode}</Text> : null}

                        </View>
                        <View style={{ width: "40%", justifyContent: "center", paddingRight: 10 }}>
                            {item.staffCode ? <Text style={[styles.headerText, { textTransform: "none" }]}>{item.staffCode}</Text> : null}
                            {item.staffName ? <Text style={{ fontFamily: "Poppins-Regular", color: BlueColor, marginVertical: 5 }}>{item.staffName}</Text> : null}

                        </View>
                    </Animatable.View>
                }) : !this.state.Loader ? <Animatable.View key={Math.random()}
                    duration={400}
                    style={[styles.header, { width: "95%", alignSelf: "center" }]}
                    transition="backgroundColor"
                >
                    <Text style={[styles.headerText, { textTransform: "none" }]}>Oops! No data avaiblabe</Text>
                </Animatable.View> : null}
            </Animatable.View>
        );
    }
    //SubjectContent(code) {
    //    return this.state.Subjects.length ? <Animatable.View key={Math.random()}
    //        duration={400}
    //        style={[styles.header, { width: "100%" }]}
    //        transition="backgroundColor"
    //    >
    //        {
    //            this.state.Subjects.map((item, index) => {

    //                return <View key={Math.random()}>
    //                    <Animatable.View key={Math.random()}
    //                        duration={400}
    //                        style={[styles.header, { width: "100%" }]}
    //                        transition="backgroundColor"
    //                    >
    //                        <TouchableOpacity activeOpacity={0.6} style={{ justifyContent: "center", alignItems: "center", flexDirection: "row", width: "100%" }} onPress={() => {
    //                            if (this.state.TempIndex == index) {
    //                                this.setState({ TempIndex: -1 })
    //                            } else {
    //                                this.setState({ TempIndex: index, filterredSubTempIndex: -1, filterredUnitNameTempIndex: -1, filterredSubUnitNameTempIndex: -1 })
    //                            }
    //                        }}>
    //                            {/*<Text style={[styles.headerText, { marginRight: 10 }]}>{this.state.TempIndex == index ? "-" : "+"}</Text>*/}
    //                            <View style={{ width: "70%", justifyContent: "center", paddingLeft: 10 }}>
    //                                {item.subjectName ? <Text style={styles.headerText}>Subject name : {item.subjectName}</Text> : null}
    //                                {item.subjectCode ? <Text style={styles.headerText}>Subject code : {item.subjectCode}</Text> : null}
    //                                {item.subjectType ? <Text style={styles.headerText}>Subject type : {item.subjectType}</Text> : null}
    //                                {item.staffName ? <Text style={styles.headerText}>Staff name : {item.staffName}</Text> : null}
    //                                {item.staffCode ? <Text style={styles.headerText}>Staff code : {item.staffCode}</Text> : null}
    //                                {item.mobile ? <Text style={styles.headerText}>Staff mobile : {item.mobile}</Text> : null}
    //                                {item.mailId ? <Text style={styles.headerText}>Staff Email : {item.mailId}</Text> : null}
    //                            </View>
    //                            <View style={{ width: "30%", borderRadius: 10, alignItems: "flex-end", justifyContent: "space-between", padding: 10 }}>
    //                                <Image style={{ width: "100%", height: 100, resizeMode: "cover", borderRadius: 10 }} source={require("../assets/demoPic.jpeg")} />
    //                                {this.state.TempIndex == index ?
    //                                    <Image style={{ width: "20%", height: 40, resizeMode: "contain", borderRadius: 10, marginTop: 20 }} source={require("../assets/upArrow.png")} />
    //                                    :
    //                                    <Image style={{ width: "20%", height: 40, resizeMode: "contain", borderRadius: 10, marginTop: 20 }} source={require("../assets/downArrow.png")} />
    //                                }
    //                            </View>
    //                        </TouchableOpacity>
    //                    </Animatable.View>
    //                    {this.state.TempIndex == index ?
    //                        //console.log("this.state.SubjectsDetail===", (this.state.SubjectsDetail[index]))
    //                        item.parentCode ?
    //                            this.renderContent(this.state.SubjectsDetail[index].parentCode, this.state.SubjectsDetail[index].subjectName, this.state.SubjectsDetail[index].topicNumber)
    //                            : <Animatable.View key={Math.random()}
    //                                duration={400}
    //                                style={[styles.header, { width: "100%", alignSelf: "center", backgroundColor: "#DCE8EE" }]}
    //                                transition="backgroundColor"
    //                            >
    //                                <Text style={[styles.headerText, { textTransform: "none", marginLeft: 10 }]}>Oops! No data avaiblabe</Text>
    //                            </Animatable.View>

    //                        : null}
    //                </View>
    //            })
    //        }
    //    </Animatable.View>
    //        : !this.state.Loader ? <Animatable.View key={Math.random()}
    //            duration={400}
    //            style={[styles.header, { width: "95%", alignSelf: "center" }]}
    //            transition="backgroundColor"
    //        >
    //            <Text style={[styles.headerText, { textTransform: "none" }]}>Oops! No data avaiblabe</Text>
    //        </Animatable.View>
    //            : null
    //}
    render() {
        const { multipleSelect, activeSections, activeSections2 } = this.state;
        return (
            <View style={{ flex: 1, backgroundColor: "white" }}>
                <Loader
                    Loading={this.state.Loader ? true : false}
                />
                <SafeAreaView style={{ flex: 1 }}>
                    {/*<CustomHeader
                        isHome={false}
                        title={this.state.Title}
                        navigation={this.props.navigation}
                    />*/}
                    <ScrollView>

                        {this.state.Title == "Student Profile" ?
                            this.state.ProfileData ?
                                <View style={{ width: "100%", flex: 1, }}>
                                    <View style={{ width: 150, height: 150, alignSelf: "center", justifyContent: "center", alignItems: "center", marginTop: 30 }}>

                                        <Image style={{ width: "100%", height: "100%", resizeMode: "contain", alignSelf: "center" }} source={{ uri: `data:image/jpeg;base64,${this.state.ProfileData.studentImage}` }} loadingIndicatorSource={require("../assets/loading2.png")} />
                                        {/*<Image style={{ width: 150, height: 150, resizeMode: "contain", borderRadius: 150, borderWidth: 1, alignSelf: "center", marginTop: 30 }} source={{ uri: this.state.ProfileData.studentImage }} loadingIndicatorSource={require("../assets/userIcon.png")} />*/}
                                    </View>
                                    {this.state.ProfileData.name ? <Text style={styles.ProfileText}>Name - {this.state.ProfileData.name}</Text> : null}
                                    {this.state.ProfileData.courseName ? <Text style={styles.ProfileText}>Course name - {this.state.ProfileData.courseName}</Text> : null}
                                    {this.state.ProfileData.currentYear ? <Text style={styles.ProfileText}>Current year - {this.state.ProfileData.currentYear ? this.state.ProfileData.currentYear : "undefined"}</Text> : null}
                                    {this.state.ProfileData.degreeCode ? <Text style={styles.ProfileText}>Degree code - {this.state.ProfileData.degreeCode}</Text> : null}
                                    {this.state.ProfileData.currentSemester ? <Text style={styles.ProfileText}>Current semester - {this.state.ProfileData.currentSemester}</Text> : null}
                                    {this.state.ProfileData.batchYear ? <Text style={styles.ProfileText}>Batch year - {this.state.ProfileData.batchYear}</Text> : null}
                                    {this.state.ProfileData.rollNo ? <Text style={styles.ProfileText}>Roll no - {this.state.ProfileData.rollNo}</Text> : null}
                                    <TouchableOpacity style={[styles.loginButton, { backgroundColor: "transparent" }]} onPress={() =>
                                        this.props.navigation.navigate("Login")
                                    }>
                                        <Text style={[styles.text, { color: config.themeColor, fontWeight: "bold" }]}>LOGOUT</Text>
                                    </TouchableOpacity>
                                </View>
                                : <Animatable.View key={Math.random()}
                                    duration={400}
                                    style={[styles.header, { width: "95%", alignSelf: "center" }]}
                                    transition="backgroundColor"
                                >
                                    <Text style={[styles.headerText, { textTransform: "none" }]}>Oops! No data avaiblabe</Text>
                                </Animatable.View>

                            : null}

                        {this.state.Title == "Subjects" ?
                            this.state.SubjectsDetail.length ?
                                this.renderHeaderOfSubject()
                                : null
                            //this.SubjectContent()

                            : null}

                        {this.state.Title == "Time Table" ?
                            <View>
                                {this.state.TempTimeTable.day == "Today" ?
                                    this.TimeTablePreRenderContent()
                                    : null}
                                {this.state.TempTimeTable.day == "Weekly" ?
                                    this.renderHeader2()
                                    : null}
                                {this.state.TempTimeTable.day == "CAM" ?
                                    this.renderHeader3()
                                    : null}
                                {this.state.TempTimeTable.day == "University" ?
                                    //this.renderHeader4()
                                    this.UniversityrenderContent(this.state.UniversityTimeTable)
                                    : null}
                            </View>

                            : null}

                        {this.state.Title == "Attendance" ?
                            this.renderAttendanceHeader()
                            : null}

                        {this.state.Title == "Internal Marks" ?

                            this.state.TempData.map((item, index) => {
                                return <View><Animatable.View key={Math.random()}
                                    duration={400}
                                    style={[styles.header, { width: "97%", alignSelf: "center", backgroundColor: this.state.TempDataTempIndex == index ? "#EFEFEF" : "#F5FCFF" }]}
                                    transition="backgroundColor"
                                >
                                    <TouchableOpacity activeOpacity={0.6} style={{ justifyContent: "center", alignItems: "center", flexDirection: "row", width: "100%" }} onPress={() => {
                                        if (this.state.TempDataTempIndex == index) {
                                            this.setState({ TempDataTempIndex: -1 })
                                        } else {
                                            this.setState({ TempDataTempIndex: index, })
                                        }
                                    }}>
                                        <View style={{ width: "90%", paddingLeft: 10, flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                                            <Text style={[styles.TimeTableText]}>{item}</Text>

                                        </View>
                                        <View style={{ width: "10%", borderRadius: 10, alignItems: "flex-end", justifyContent: "space-between", padding: 10 }}>
                                            {this.state.TempDataTempIndex == index ?
                                                <Image style={{ width: "60%", height: 20, resizeMode: "contain", borderRadius: 10, marginTop: 20 }} source={require("../assets/upArrow.png")} />
                                                :
                                                <Image style={{ width: "60%", height: 20, resizeMode: "contain", borderRadius: 10, marginTop: 20 }} source={require("../assets/downArrow.png")} />
                                            }
                                        </View>
                                    </TouchableOpacity>
                                </Animatable.View>
                                    {item == "Exam Wise" && this.state.TempDataTempIndex == index ?
                                        this.renderInternalMarksHeader()
                                        : null}
                                    {item == "Subject Wise" && this.state.TempDataTempIndex == index ?
                                        this.renderInternalPerformanceSubHeader()
                                        : null}
                                </View>
                            })

                            : null}

                        {this.state.Title == "University Marks" ?
                            this.renderUniversityMarksHeader()
                            : null}
                        {this.state.Title == "Performance" ?
                            this.renderPerformanceHeader()
                            : null}

                    </ScrollView>
                </SafeAreaView>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
        //paddingTop: Constants.statusBarHeight,
    },
    title: {
        textAlign: 'center',
        fontSize: 22,
        fontWeight: '300',
        marginBottom: 20,
    },
    header: {
        backgroundColor: '#F5FCFF',
        padding: 10,
        width: "100%",
        alignSelf: "center",
        borderRadius: 5,
        margin: 5
    },
    headerSubContent: {
        backgroundColor: '#F5FCFF',
        //padding: 10,
        width: "100%",
        //margin: -5,
        alignSelf: "center",
        marginBottom: 5
    },
    headerText: {
        //textAlign: 'center',
        fontSize: ResponsiveSize(12),
        fontWeight: '500',
        textTransform: "capitalize"

    },
    subContent: {
        justifyContent: "flex-start",
        alignItems: "center",
        flexDirection: "row",
        width: "100%"
    },
    content: {
        padding: 20,
        backgroundColor: '#fff',
    },
    content2: {
        padding: 20,
        marginLeft: 10,
        backgroundColor: '#fff',
    },
    active: {
        backgroundColor: 'rgba(255,255,255,1)',
    },
    inactive: {
        backgroundColor: 'rgba(245,252,255,1)',
    },
    selectors: {
        marginBottom: 10,
        flexDirection: 'row',
        justifyContent: 'center',
    },
    selector: {
        backgroundColor: '#F5FCFF',
        padding: 10,
    },
    activeSelector: {
        fontWeight: 'bold',
    },
    selectTitle: {
        fontSize: 14,
        fontWeight: '500',
        padding: 10,
    },
    multipleToggle: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginVertical: 30,
        alignItems: 'center',
    },
    multipleToggle__title: {
        fontSize: 16,
        marginRight: 8,
    },
    TextStyle: {
        alignSelf: "center",
        marginTop: 40,
        fontSize: ResponsiveSize(18)
    },
    ProfileText: {
        alignSelf: "center",
        marginTop: 10,
        fontWeight: "500",
        fontSize: ResponsiveSize(16)
    },
    TimeTableText: {
        //alignSelf: "center",
        fontWeight: "500",
        fontSize: ResponsiveSize(16)
    },
    loginButton: {
        width: "40%",
        backgroundColor: config.themeColor,
        //paddingHorizontal: 50,
        borderRadius: 5,
        justifyContent: "center",
        alignItems: "center",
        height: 45,
        alignSelf: "center",
        marginTop: 40
    },
    text: {
        textAlign: "center",
        color: "white",
        fontSize: ResponsiveSize(config.buttonSize)
    },


    SubjectMainContainer: {
        width: "100%",
        alignItems: "center"
    },
    SubjectNameImageView: {
        width: "95%",
        alignSelf: "center",
        height: 60,
        resizeMode: "stretch",
        position: "relative",
        borderRadius: 15,
        marginVertical: 10
    },
    SubjectNameTextView: {
        height: 60,
        width: "90%",
        justifyContent: "center",
        position: "absolute",
        marginVertical: 10,
        alignItems: "center"
    },
    SubjectNameTextStyle: {
        color: "white",
        fontWeight: "600",
        fontSize: ResponsiveSize(13),
        paddingHorizontal: 10
    },
    StaffDetailTextStyle: {
        color: "black",
        lineHeight: 20
    },
    SubjectNameContainer: {
        alignSelf: "center",
        width: "95%",
        borderRadius: 15,
        paddingBottom: 10
    },
})