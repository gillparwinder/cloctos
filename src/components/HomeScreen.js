import React, { Component } from "react";
import { Image, TouchableOpacity, SafeAreaView, View, Text, Dimensions, ScrollView, StyleSheet } from "react-native";
import { CustomHeader } from "./CustomHeader";
import { Loader } from "./Loader";
import { SimpleAnimation } from 'react-native-simple-animations';
import { Fab } from "native-base";
import config from "../config/config";
import Toast from 'react-native-tiny-toast';
import ResponsiveSize from "../config/ResponsiveSize";
import { FlatList } from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import BottomTab from "./BottomTab";
import DetailScreen from "./DetailScreen";
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
const ToastData = {
    position: Toast.position.TOP,
    imgSource: require('../assets/alert.png'),
    imgStyle: { width: 40, height: 40 },
};
export default class HomeScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Loader: false,
            Token: "",
            InstituteCode: "",
            Active: "home",
            isValidUser: false,
            BannerArray: [
                { image: "https://i.stack.imgur.com/9WYxT.png", name: "Student Profile" },
                { image: "https://i.stack.imgur.com/9WYxT.png", name: "Subjects" },
                { image: "https://i.stack.imgur.com/9WYxT.png", name: "Time Table" },
                { image: "https://i.stack.imgur.com/9WYxT.png", name: "Attendance" },
                { image: "https://i.stack.imgur.com/9WYxT.png", name: "Internal Marks" },
                { image: "https://i.stack.imgur.com/9WYxT.png", name: "University Marks" },
                { image: "https://i.stack.imgur.com/9WYxT.png", name: "Performance" },
            ],
        }
    }
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: "white", justifyContent: "flex-end", paddingBottom: 15 }}>
                <Loader
                    Loading={this.state.Loader ? true : false}
                />
                {/*{this.state.isValidUser ?*/}
                <SafeAreaView style={{ flex: 1, justifyContent: "space-between" }}>
                    {/*
                    <CustomHeader
                        isHome={true}
                        title="Homescreen"
                        notification={true}
                        navigation={this.props.navigation}
                    />*/}

                    <FlatList
                        data={this.state.BannerArray}
                        renderItem={({ item }) => (
                            <TouchableOpacity onPress={() => {
                                //this.setState({ KeyName: item.name })
                                item.name == "Subjects" ?
                                    this.props.navigation.navigate("SubjectList", { title: item.name })
                                    :
                                    item.name == "Time Table" ?
                                        this.props.navigation.navigate("TimeTable", { title: item.name })
                                        :
                                        this.props.navigation.navigate("DetailScreenMenu", { title: item.name })
                            }} style={styles.FlatlistChildView}>
                                <Image style={styles.ChildViewImage} source={{ uri: item.image }} />
                                <Text style={styles.ChildViewText}>{item.name}</Text>
                            </TouchableOpacity>
                        )}
                        //Setting the number of column
                        numColumns={2}
                        keyExtractor={(item, index) => index}
                    />



                    <View style={{ width: "100%", height: 120, backgroundColor: "transparent" }} />

                </SafeAreaView>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    FlatlistChildView: {
        width: "45%",
        height: 100,
        borderRadius: 10,
        flexDirection: 'column',
        backgroundColor: "#383E7B",
        margin: 10,
        justifyContent: "center",
        alignItems: "center"
    },
    ChildViewText: {
        fontSize: ResponsiveSize(16),
        color: "white",
        fontWeight: "500",
        fontFamily: "Poppins-Regular"
    },
    ChildViewImage: {
        justifyContent: 'center',
        alignItems: 'center',
        height: "100%",
        width: "100%",
        position: "absolute",
        borderRadius: 10,
    },
})