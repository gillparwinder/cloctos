import React, { Component } from 'react';
import { Thumbnail, Card, Header } from 'native-base';
import { Text, View, TouchableOpacity, StatusBar } from 'react-native';
import { Overlay } from 'react-native-elements';
import { ActivityIndicator } from 'react-native';
import { Image } from 'react-native';
export class Loader extends Component {
    render() {
        let { navigation, Loading, } = this.props
        return (
            Loading ?
                <View style={{ height: "100%", width: "100%", position: "absolute", zIndex: 20, justifyContent: "center", alignItems: "center", backgroundColor: 'rgba(0, 0, 0, .2)' }}>
                    {/*<View style={{ height: "100%", width: "100%", position: "absolute", zIndex: 20, justifyContent: "center", alignItems: "center", backgroundColor: 'transparent' }}>*/}
                    <View style={{
                        justifyContent: "space-evenly", alignItems: "center", width: 150, minHeight: 100, maxHeight: 200, backgroundColor: "white",
                        borderRadius: 15, shadowColor: "gray", shadowOffset: { width: 1, height: 1 }, shadowOpacity: .7, elevation: 5
                    }} >
                        {/*<ActivityIndicator style={{ marginVertical: 10 }} size="large" color="red" />*/}
                        {/*<Text style={{fontFamily: "Poppins-Regular", marginTop: 10 }} onPress={() => {
                            //alert("DSFsdgsd")
                        }}>Loading...</Text>*/}
                        <Image source={require("../assets/timeLoader.gif")} style={{ height: 40, width: 100, resizeMode: "contain" }} ></Image>
                        <Image source={require("../assets/loading.gif")} style={{ height: 40, width: 100, resizeMode: "contain" }} ></Image>
                    </View>
                </View>
                : null
        );
    }
}