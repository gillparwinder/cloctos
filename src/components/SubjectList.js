import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { Component } from "react";
import { StyleSheet, ScrollView, } from "react-native";
import { Text } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import * as Animatable from 'react-native-animatable';
import Toast from 'react-native-tiny-toast';
import ResponsiveSize from "../config/ResponsiveSize";
import config from "../config/config";
import { ImageBackground } from "react-native";
import { SafeAreaView } from "react-native";
import { CustomHeader } from "./CustomHeader";
import { Loader } from "./Loader";
import { View } from "react-native";
const ToastData = {
    position: Toast.position.TOP,
    imgSource: require('../assets/alert.png'),
    imgStyle: { width: 40, height: 40 },
};

export default class SubjectList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Loader: true,
            Subjects: [],
            colorCode: [
                "#23C4D7",
                "#8F98FF",
                "#7260E9",
                "#23C4D7",
                "#8F98FF",
                "#7260E9",
                "#23C4D7",
                "#8F98FF",
                "#7260E9",
                "#23C4D7",
                "#8F98FF",
                "#7260E9",
                "#23C4D7",
                "#8F98FF",
                "#7260E9",
                "#23C4D7",
                "#8F98FF",
                "#7260E9",
                "#23C4D7",
                "#8F98FF",
                "#7260E9",
            ],
            BackgroundImageArr: [
                require("../assets/greenBg.png"),
                require("../assets/blueBg.png"),
                require("../assets/purpleBg.png"),
                require("../assets/greenBg.png"),
                require("../assets/blueBg.png"),
                require("../assets/purpleBg.png"),
                require("../assets/greenBg.png"),
                require("../assets/blueBg.png"),
                require("../assets/purpleBg.png"),
                require("../assets/greenBg.png"),
                require("../assets/blueBg.png"),
                require("../assets/purpleBg.png"),
                require("../assets/greenBg.png"),
                require("../assets/blueBg.png"),
                require("../assets/purpleBg.png"),
                require("../assets/greenBg.png"),
                require("../assets/blueBg.png"),
                require("../assets/purpleBg.png"),
                require("../assets/greenBg.png"),
                require("../assets/blueBg.png"),
                require("../assets/purpleBg.png"),
            ],
            Title: this.props.route.params ? (this.props.route.params.title ? this.props.route.params.title : "") : "",
        }
    }
    componentDidMount() {
        this.retrieveData();

    }
    retrieveData = async () => {
        try {
            const value = await AsyncStorage.getItem('token');
            if (value !== null) {
                //alert(value);
                this.setState({ Token: value }, function () {
                    this.onLoadGetSubjects()
                })
            }
        } catch (error) {
            alert('Error retrieving data');
        }
    }
    onLoadGetSubjects = () => {
        const url = config.baseUrl + "student/StudentSubjectList";
        console.log(url);
        fetch(url, {
            method: 'GET',
            headers: {
                "Accept": 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.state.Token
            },
            //body: JSON.stringify({
            //    "mpin": this.state.Mpin
            //}),
        })
            .then(response => response.json())
            .then(responseJson => {
                //alert(JSON.stringify(responseJson))
                console.log(responseJson);
                if (responseJson) {

                    this.setState({ Loader: false, Subjects: responseJson }, function () {

                    })

                } else {
                    this.setState({ Loader: false }, function () {
                        Toast.show(responseJson.title ? responseJson.title : 'Temporary error try again after some time', ToastData);
                    })
                }
            })
            .catch(error => {
                this.setState({ Loader: false }, function () {
                    //alert(JSON.stringify(error))
                    console.log(error);
                    Toast.show("error", ToastData);
                })
            });
    }
    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                {this.state.Title ?
                    <CustomHeader
                        isHome={false}
                        title={this.state.Title}
                        navigation={this.props.navigation}
                    />
                    : null}
                <Loader
                    Loading={this.state.Loader ? true : false}
                />

                <ScrollView>
                    {this.state.Subjects.length ?
                        <Animatable.View key={Math.random()}
                            duration={400}
                            style={[styles.headerNew]}
                            transition="backgroundColor"
                        >
                            {
                                this.state.Subjects.map((item, index) => {

                                    return item.subjectName ? <ImageBackground style={{ width: "100%", height: 100, resizeMode: "cover", marginBottom: 15 }} source={index < 20 ? this.state.BackgroundImageArr[index] : (this.state.BackgroundImageArr[Math.floor(Math.random() * this.state.BackgroundImageArr.length)])} key={Math.random()}>
                                        {/*<ImageBackground style={{ width: "100%", height: 100, resizeMode: "cover", marginBottom: 15 }} source={this.state.BackgroundImageArr[Math.floor(Math.random() * this.state.BackgroundImageArr.length)]} key={Math.random()}>*/}

                                        <TouchableOpacity activeOpacity={.5} style={{ width: "100%", height: "100%", justifyContent: "flex-end", padding: 20 }} onPress={() => {
                                            //console.log({ colorCode:  this.state.colorCode[index]})
                                            this.props.navigation.navigate("DetailScreenMenu", { Subjects: item, title: "Subjects", colorCode: index < 20 ? this.state.colorCode[index] : (this.state.colorCode[Math.floor(Math.random() * this.state.colorCode.length)]), colorPic: this.state.BackgroundImageArr[index] })
                                        }}>
                                            <Text style={styles.headerTextNew}>{item.subjectName}</Text>
                                        </TouchableOpacity>
                                    </ImageBackground>

                                        : null
                                })
                            }
                        </Animatable.View>
                        : !this.state.Loader ? <Animatable.View key={Math.random()}
                            duration={400}
                            style={[styles.headerNew, { width: "95%", alignSelf: "center" }]}
                            transition="backgroundColor"
                        >
                            <Text style={[styles.headerTextNew, { textTransform: "none" }]}>Oops! No data avaiblabe</Text>
                        </Animatable.View>
                            : null}
                    <View style={{ width: "100%", height: 120, backgroundColor: "transparent" }} />
                </ScrollView>
            </SafeAreaView>
        )
    }
}
const styles = StyleSheet.create({

    headerNew: {
        padding: 10,
        width: "100%",
        alignSelf: "center",
        borderRadius: 5,
    },
    headerTextNew: {
        //textAlign: 'center',
        width: "60%",
        fontSize: ResponsiveSize(16),
        fontWeight: '400',
        lineHeight: 24,
        textTransform: "capitalize",
        color: "white",
        fontFamily: "Poppins-Regular"

    },

})