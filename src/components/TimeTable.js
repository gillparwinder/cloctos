import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { Component } from "react";
import { StyleSheet, ScrollView, } from "react-native";
import { Text, View } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import * as Animatable from 'react-native-animatable';
import Toast from 'react-native-tiny-toast';
import ResponsiveSize from "../config/ResponsiveSize";
import config from "../config/config";
import { ImageBackground } from "react-native";
import { SafeAreaView } from "react-native";
import { CustomHeader } from "./CustomHeader";
import { Loader } from "./Loader";
const ToastData = {
    position: Toast.position.TOP,
    imgSource: require('../assets/alert.png'),
    imgStyle: { width: 40, height: 40 },
};

export default class TimeTable extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Loader: false,
            TempTimeTable: [
                { day: "Today" },
                { day: "Weekly" },
                { day: "CAM" },
                { day: "University" },
            ],
            BackgroundImageArr: [
                require("../assets/greenBg.png"),
                require("../assets/blueBg.png"),
                require("../assets/purpleBg.png"),
                require("../assets/greenBg.png"),
                require("../assets/blueBg.png"),
                require("../assets/purpleBg.png"),
                require("../assets/greenBg.png"),
                require("../assets/blueBg.png"),
                require("../assets/purpleBg.png"),
                require("../assets/greenBg.png"),
                require("../assets/blueBg.png"),
                require("../assets/purpleBg.png"),
                require("../assets/greenBg.png"),
                require("../assets/blueBg.png"),
                require("../assets/purpleBg.png"),
                require("../assets/greenBg.png"),
                require("../assets/blueBg.png"),
                require("../assets/purpleBg.png"),
            ],
            Title: this.props.route.params ? (this.props.route.params.title ? this.props.route.params.title : "") : "",
        }
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                {this.state.Title ?
                    <CustomHeader
                        isHome={false}
                        title={this.state.Title}
                        navigation={this.props.navigation}
                    />
                    : null}
                <Loader
                    Loading={this.state.Loader ? true : false}
                />
                <ScrollView>
                    <Animatable.View key={Math.random()}
                        duration={400}
                        style={[styles.headerNew]}
                        transition="backgroundColor"
                    >
                        {
                            this.state.TempTimeTable.map((item, index) => {

                                return <ImageBackground style={{ width: "100%", height: 100, resizeMode: "cover", marginBottom: 15 }} source={index < 18 ? this.state.BackgroundImageArr[index] : (this.state.BackgroundImageArr[Math.floor(Math.random() * this.state.BackgroundImageArr.length)])} key={Math.random()}>
                                    <TouchableOpacity activeOpacity={.5} style={{ width: "100%", height: "100%", justifyContent: "center", paddingHorizontal: 20 }} onPress={() => {
                                        this.props.navigation.navigate("DetailScreenMenu", { TimeTable: item, title: "Time Table" })
                                    }}>
                                        <Text style={styles.headerTextNew}>{item.day}</Text>
                                    </TouchableOpacity>
                                </ImageBackground>
                            })
                        }
                    </Animatable.View>
                    <View style={{ width: "100%", height: 120, backgroundColor: "transparent" }} />

                </ScrollView>
            </SafeAreaView>
        )
    }
}
const styles = StyleSheet.create({

    headerNew: {
        padding: 10,
        width: "100%",
        alignSelf: "center",
        borderRadius: 5,
    },
    headerTextNew: {
        //textAlign: 'center',
        fontSize: ResponsiveSize(20),
        fontWeight: '500',
        lineHeight: 30,
        textTransform: "capitalize",
        color: "white"

    },

})