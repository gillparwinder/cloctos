export default {
    themeColor: "#131364",
    SubThemeColor: "#7265E6",
    bgColor: "#F9F9F9",
    baseUrl: "http://insproplus.com/studentapi/api/",
    buttonSize: 16,
    TextInputTextSize: 18,
    textSize: 18,
    headerTextSize: 20,
    BottomTabBarColor: "#120E66"
}